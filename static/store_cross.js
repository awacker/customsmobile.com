function get_Key_from_obj(obj) {
	var key = obj.parentNode.previousElementSibling.previousElementSibling.firstChild.href
	return key.split('docview?doc_id=')[1]
}

function get_node_content_by_obj(obj) {
	var node = obj.parentNode.parentNode.parentNode.parentNode.outerHTML;
	$('.ghost').html(node);
	$('.ghost .fa-slack').parent().remove();
	font_size_default('.ghost');
	return $('.ghost').html();
}

function save_to_Storege(obj) {
	if ( $('#list_flag').length == 0 ) {
		var doc_id = getURLParameter('doc_id');
		var key = getReal_doc_id(doc_id);
		font_size_default('.ghost');
		var content = $('.ghost').html();		
		localStorage.setItem('cross_'+key, content);
	} else {
		var doc_id = get_Key_from_obj(obj);
		var key = getReal_doc_id(doc_id);		
		var content = get_node_content_by_obj(obj);
		localStorage.setItem('cross_'+key, content);
	}
}

function remove_from_Storege(obj) {
	if ( $('#list_flag').length == 0 ) {
		var doc_id = getURLParameter('doc_id');
		var key = getReal_doc_id(doc_id);
		localStorage.removeItem('cross_'+key);
	} else {
		var doc_id = get_Key_from_obj(obj);
		var key = getReal_doc_id(doc_id);
		localStorage.removeItem('cross_'+key);	
	}
}

function clickStar(e) {
	if ( $(e.target).hasClass( "fa-star-o" ) ) {
		$(e.target).removeClass("fa-star-o");
		$(e.target).addClass("fa-star");
		save_to_Storege(e.target);
	} else {
		$(e.target).removeClass("fa-star");
		$(e.target).addClass("fa-star-o");
		remove_from_Storege(e.target);
	}
}

function get_Key_from_element(element) {
	var href = $(element).find('.link-s').attr('href');
	return href.split('docview?doc_id=')[1]
}

function get_Star_from_element(element) {
	return $(element).find('.fa-star-o')[0];
}

function getURLParameter(name) {
	return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}

function getReal_doc_id(doc_id) {
	var v = doc_id.replace('%20',' ').split(' ')
	return v[v.length-1].toLowerCase();
}

function read_stored_elements() {
	if ( $('#list_flag').length == 0 ) {
		var doc_id = getURLParameter('doc_id');
		var real_doc_id = getReal_doc_id(doc_id);
		if ( localStorage.getItem('cross_'+real_doc_id) ) {
			$('.fa-star-o').addClass("fa-star");
			$('.fa-star-o').removeClass("fa-star-o");
		}	
//		console.log(doc_id);
		$.ajax({
	        url:"/cross/docheade444r",
	        type: 'GET',
	        dataType: 'json',
	        data:{doc_id: '"'+doc_id+'"'},
	        error: function(){   
	        	console.log('/cross/docheader ERROR!!!');
	        },
	        success: function (data) { 

	        	if ( data.id && data.date ) {
	        		$("#header_doc_id").html('<i class="fa fa-tag"> </i> '+data.id);
		        	$("#header_calendar").html('<i class="fa fa-calendar"> </i> ('+data.date+') ');	
		        	$(".ghost #category").html(' Type: '+data.category+' / HTSUS: <a href="#"> '+data.tariff_no+' </a>');
		        	$(".ghost #related").html(' / Related: '+data.related);
		        	$(".ghost #ruling_reference").html(' '+data.ruling_reference+' ');
	        	}
	        	
				$(".ghost #doc_id").html('<i class="fa fa-tag"> </i> '+$("#header_doc_id").text());
				$(".ghost #date").html('<i class="fa fa-calendar"> </i> ('+$("#header_calendar").text()+') ');	        	
	        	$(".link-s").attr("href",location.href);
	        	
	        }
	 	});	
	} else {
		$('.list_box').each(function(i,element) {
			var doc_id = get_Key_from_element(element);
			var key = getReal_doc_id(doc_id);
			if ( localStorage.getItem('cross_'+key) ) {
				var star = get_Star_from_element(element)
				$(star).addClass("fa-star");
				$(star).removeClass("fa-star-o");

			}
		});			
	}
	read_font_size();
}

function startsWith(str, s) {
	return str.lastIndexOf(s, 0) === 0
};

function showStarred(e) {

	$('.main_container').remove();
	$('<div class="col-xs-12 main_container"><div id="stareed_content" class="row abt"></div></div>').insertAfter( ".white_bar" );
	$('#doc_container').remove();
	

	for ( var i = 0, len = localStorage.length; i < len; ++i ) {
		
		if ( startsWith( localStorage.key(i), 'cross_')) {
			$('#stareed_content').append(localStorage.getItem(localStorage.key(i)))
		}
	}
	$('i.fa-star-o, i.fa-star').bind('click', (function(e){clickStar(e)}));
//	$('i.fa-tag').parent().bind('click', (function(e){viewDoc(e)}));
	$('i.fa-envelope-o').bind('click', (function(e){mailSend(e)}));
//	read_stored_elements()
	$('body').append('<div id="list_flag"></div>');	
	font_size_hand();
	read_font_size();
	$('#subcaption').html(' - Show Starred');
}

function clearStarred(e) {
	for ( var key in localStorage ) {	
		if ( startsWith( key, 'cross_')) {
			localStorage.removeItem(key);
		}
	}
	$('i.fa-star, i.fa-star-o').addClass("fa-star-o");
}

function get_Key_from_obj_1(obj) {
	var key = obj;
	return key.href.split('docview/')[1];
}

function get_node_content_by_obj_1(obj) {
	return obj.parentNode.parentNode.parentNode.parentNode.outerHTML;
}

function get_Starred_Item_text(element) {
	var caption = myTrim($(element).find('.link-s').text());
	var link = 'http://'+location.hostname + $(element).find('.link-s').attr('href').replace(' ','%20');
	var date = $(element).find('.fa-calendar').parent().text();	
	var re = myTrim($(element).find('p').text());
	return caption + '  |  ' + date + '  |  ' + link + '\n' + re; 
}

function get_signature() {
	var signature = '\n--\n';
	signature += 'Thank you for visiting CustomsMobile!\n';
	signature += '"The Only Customs Website You\'ll Ever Need."\n';
	signature += 'http://www.customsmobile.com';
	return signature
}

function mailStarred(e) {
	var a = e.target;
	$('.ghost').html('');
	for ( var i = 0, len = localStorage.length; i < len; ++i ) {
		
		if ( startsWith( localStorage.key(i), 'cross_')) {
			$('.ghost').append(localStorage.getItem(localStorage.key(i)))
		}
	}
	var body = '';
	$('.ghost .list_box').each(function(i,element) {
		body += get_Starred_Item_text(element);
		body += '\n===================================================================\n\n'
	});		
	
	$(a).attr("href", 'mailto:?subject=RULINGS from customsmobile.com&body='+encodeURIComponent(body+get_signature()));
}

function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}

function mailSend(e) {
	if ( $('#list_flag').length == 0 ) {
		
		
		var a = e.target.parentNode;	
		var caption = myTrim($("#header_doc_id").text());
		var link = location.href; //'http://'+location.hostname + $("#header_doc_id").attr("href").replace(' ','%20');// 'http://'+location.hostname + $(a.parentNode.parentNode).find('.link-s').attr('href');
		var date = $("#header_calendar").text();	
		var re = $(".ghost #ruling_reference").text();
		var body = caption + '  |  ' + date + '  |  ' + link + '\n' + re + '\n'; 
		$(a).attr("href", 'mailto:?subject=RULINGS from customsmobile.com ('+caption+')&body='+encodeURIComponent(body+get_signature()));		
		
	} else {
		
		var a = e.target.parentNode;
		console.log(a.parentNode.parentNode);
		var caption = myTrim($(a.parentNode.parentNode).find('.link-s').text());
		var link = 'http://'+location.hostname + $(a.parentNode.parentNode).find('.link-s').attr('href').replace(' ','%20');
		var date = $(a.parentNode.parentNode).find('.fa-calendar').parent().text();	
		var re = myTrim($(a.parentNode.parentNode.parentNode).find('p').text());
		var body = caption + '  |  ' + date + '  |  ' + link + '\n' + re + '\n'; 
		$(a).attr("href", 'mailto:?subject=RULINGS from customsmobile.com ('+caption+')&body='+encodeURIComponent(body+get_signature()));		
	}
}

$(document).ready(function() {

//	read_stored_elements()
    $('i.fa-star-o, i.fa-star').bind('click', (function(e){clickStar(e)}));
	$('i.fa-envelope-o').bind('click', (function(e){mailSend(e)}));
	$('.fa-eye').parent().bind('click', (function(e){showStarred(e)}));
	$('.fa-envelope').parent().bind('click', (function(e){mailStarred(e)}));
	$('.fa-trash-o').parent().bind('click', (function(e){clearStarred(e)}));

});

