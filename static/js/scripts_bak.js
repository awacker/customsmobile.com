/*global jQuery:false */
jQuery(function($) {

    var CustomsMobile = window.CustomsMobile || {};

    CustomsMobile.init = function() {
		
		//Font size init
		font_size_hand();
		read_font_size();
		$('.jfontsize-button').bind('click', (function(e){write_font_size()}));
		
		
		// Menu settings
		$('#menuToggle, .menu-close').on('click', function() {
			$('#menuToggle').toggleClass('active');
			$('body').toggleClass('body-push-toleft');
			$('#theMenu').toggleClass('menu-open');
		});
		$('#menuToggleright, .menu-closeright').on('click', function() {
			$('#menuToggleright').toggleClass('active');
			$('body').toggleClass('body-push-toright');
			$('#theMenuright').toggleClass('menu-open');
		});
		
		
    };
	


    $(document).ready(function() {

        CustomsMobile.init();

     

        $(window).scroll(function() {

        });

        $(window).load(function() {

			if ($('.fb-like-box').length){
				var h=$(window).height();
				if ($('body').height()<=h){
					var int=setInterval(test, 1000)
					function test(){
						var s=$('.fb_iframe_widget span');
						var temph=$(window).height()-$('.top_bar').outerHeight() - $('.blue_bar').outerHeight() -60;
						s.height(temph);
						var int = setTimeout(arguments.callee, 1000);
						 if (s.length){
							window.clearInterval(int);
							return false;
						 }
						 
					};
				}
			}
		});

        $(window).bind('orientationchange resize', function() {
        });
    });

});