/*
 * jQuery jFontSize Plugin
 * Examples and documentation: http://jfontsize.com
 * Author: Frederico Soares Vanelli
 *         fredsvanelli@gmail.com
 *         http://twitter.com/fredvanelli
 *         http://facebook.com/fred.vanelli
 *
 * Copyright (c) 2011
 * Version: 1.0 (2011-07-15)
 * Dual licensed under the MIT and GPL licenses.
 * http://jfontsize.com/license
 * Requires: jQuery v1.2.6 or later
 */

(function($){
    $.fn.jfontsize = function(opcoes) {

        var $this=$(this);
	    var defaults = {
		    btnMinusClasseId: '#jfontsize-minus',
		    btnDefaultClasseId: '#jfontsize-default',
		    btnPlusClasseId: '#jfontsize-plus',
            btnMinusMaxHits: 10,
            btnPlusMaxHits: 10,
            sizeChange: 1
	    };

        if (opcoes){opcoes = $.extend(defaults, opcoes)};

        var limite=new Array();
        var fontsize_padrao=new Array();

        $(this).each(function(i){
            limite[i]=0;
            fontsize_padrao[i];
        });

        $(opcoes.btnMinusClasseId+', '+opcoes.btnDefaultClasseId+', '+opcoes.btnPlusClasseId).removeAttr('href');
        $(opcoes.btnMinusClasseId+', '+opcoes.btnDefaultClasseId+', '+opcoes.btnPlusClasseId).css('cursor', 'pointer');

        /* A��o do Bot�o A- */
        $(opcoes.btnMinusClasseId).click(function(){
            $(opcoes.btnPlusClasseId).removeClass('jfontsize-disabled');
            $this.each(function(i){
                    fontsize_padrao[i]=$(this).css('font-size');
                    fontsize_padrao[i]=fontsize_padrao[i].replace('px', '');   
                    fontsize=$(this).css('font-size');
                    fontsize=parseInt(fontsize.replace('px', ''));
                //    console.log(fontsize);
                    if ( fontsize > 12 ) {
                        fontsize=fontsize-(opcoes.sizeChange);
                        fontsize_padrao[i]=fontsize_padrao[i]-(limite[i]*opcoes.sizeChange);
                        limite[i]--;
                        $(this).css('font-size', fontsize+'px'); 	
                        $(opcoes.btnMinusClasseId).addClass('jfontsize-disabled');               	
                    }
            })
        });

        /* A��o do Bot�o A */
        $(opcoes.btnDefaultClasseId).click(function(){
            $(opcoes.btnMinusClasseId).removeClass('jfontsize-disabled');
            $(opcoes.btnPlusClasseId).removeClass('jfontsize-disabled');
            $this.each(function(i){
                limite[i]=0;
                $(this).css('font-size', fontsize_padrao[i]+'px');
            })
        });

        /* A��o do Bot�o A+ */
        $(opcoes.btnPlusClasseId).click(function(){
        
            $(opcoes.btnMinusClasseId).removeClass('jfontsize-disabled');
            $this.each(function(i){
                    fontsize_padrao[i]=$(this).css('font-size');
                    fontsize_padrao[i]=fontsize_padrao[i].replace('px', '');
                    fontsize=$(this).css('font-size');
                    fontsize=parseInt(fontsize.replace('px', ''));
                    if ( fontsize < 35 ) {
                        fontsize=fontsize+opcoes.sizeChange;
                        fontsize_padrao[i]=fontsize_padrao[i]-(limite[i]*opcoes.sizeChange);
                        limite[i]++;
                        $(this).css('font-size', fontsize+'px');
                        $(opcoes.btnPlusClasseId).addClass('jfontsize-disabled');                   	
                    }
            })
        });
    };
})(jQuery);

function font_size_hand() {
	var nodes = $("#font_size_attributes").text();
	$(nodes).jfontsize({   //'.abt,.list_box li,p,span'
		btnMinusClasseId: '#jfontsize-m2',
		btnDefaultClasseId: '#jfontsize-d2',
		btnPlusClasseId: '#jfontsize-p2',
		btnMinusMaxHits: 1,
		btnPlusMaxHits: 5,
		sizeChange: 5
		});
}

function font_size_default(target) {
//	console.log('sss');
	var nodes = $("#font_size_attributes").text();
	console.log(nodes); //'.abt li,p,tr,span'
	$(target).find(nodes).each(function(i){
		console.log(this);
		$(this).removeAttr("style");
	});
}

function write_font_size() {
	var nodes = $("#font_size_attributes").text();
	$(nodes).each(function(i){ //'.abt,.list_box li,p,tr,span'
		var s = $(this).attr("style");				
		localStorage.setItem('font_size', s);
		return false;
	});
}

function read_font_size() {
//	console.log('ffff');
	var s = localStorage.getItem('font_size')
	if ( !s || s == 'undefined' ) {
		return;
	}
	var nodes = $("#font_size_attributes").text();

	$(nodes).each(function(i){  //'.abt,.list_box li,p,tr,span'
		$(this).attr("style", s + ' ' + $(this).attr("style"));
	});
	
}
