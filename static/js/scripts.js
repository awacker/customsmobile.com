/*global jQuery:false */
jQuery(function($) {

    var CustomsMobile = window.CustomsMobile || {};

    CustomsMobile.init = function() {
		
		//Font size init
		font_size_hand();
		read_font_size();
		$('.jfontsize-button').bind('click', (function(e){write_font_size()}));
		
		
		// Menu settings
		$('#menuToggle, .menu-close').on('click', function() {
			$('#menuToggle').toggleClass('active');
			$('body').toggleClass('body-push-toleft');
			$('#theMenu').toggleClass('menu-open');
		});
		$('#menuToggleright, .menu-closeright').on('click', function() {
			$('#menuToggleright').toggleClass('active');
			$('body').toggleClass('body-push-toright');
			$('#theMenuright').toggleClass('menu-open');
		});
		
		
    };
	


    $(document).ready(function() {

        CustomsMobile.init();

     

        $(window).scroll(function() {

        });


        $(window).bind('orientationchange resize', function() {
        });
    });

});
