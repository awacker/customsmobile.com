function get_node_content_by_obj(obj) {
	var node = obj.parentNode.parentNode.parentNode.parentNode.outerHTML;
	$('.ghost').html(node);
	$('.ghost .fa-tag').parent().remove();
	font_size_default('.ghost');
	return $('.ghost').html();
}

function get_node_content_fromDocPage(key) {
	$('.ghost').html('');
	$('.ghost').html($('.list_box').clone());
	$('.ghost .fa-tag').parent().remove();
	var title = $( "b:contains('Title : ')" ).text().split('Title : ')[1];
	var description = $('pre').text().split(' ', 32).join(' ')+' ...';
	$( ".ghost b:contains('Title : ')" ).remove();
	$('.ghost pre').replaceWith('<p><b>'+title+'</b><br>'+description+'</p>');
	var c = '<div class="col-xs-1 padding_zero text-right"><h1><a href="/notices/docview/'+key+'"> <i class="fa fa-angle-right"></i> </a></h1></div>'
	$('.ghost .link-s').attr('href','/notices/docview/'+key);
	$('.ghost .list_box').append(c);
	$('.ghost .inner_page').attr('class','col-xs-11 padding_zero');
	font_size_default('.ghost');
	return $('.ghost').html();
}

function get_Key_from_obj_1(obj) {
	var key = obj;
	return key.href.split('docview/')[1];
}

function get_Key_from_obj(obj) {
	var key = obj.parentNode.parentNode; 
	return $(key).find('.link-s').attr('href').split('docview/')[1]
}

function save_to_Storege(obj) {
	if ( $('#list_flag').length == 0 ) {
		var key = get_Key_from_obj(obj);
		if ( !key ) {
			key = getKeyFromURL();
		}
		var content = get_node_content_fromDocPage(key);

		localStorage.setItem('csms_'+key, content);
	} else {
		var key = get_Key_from_obj(obj);
		var content = get_node_content_by_obj(obj);
		localStorage.setItem('csms_'+key, content);
	}
}

function remove_from_Storege(obj) {
	if ( $('#list_flag').length == 0 ) {
		var key = getKeyFromURL();
		localStorage.removeItem('csms_'+key);
	} else {
		var key = get_Key_from_obj(obj);
		localStorage.removeItem('csms_'+key);	
	}
}

function clickStar(e) {
	if ( $(e.target).hasClass( "fa-star-o" ) ) {
		$(e.target).removeClass("fa-star-o");
		$(e.target).addClass("fa-star");
		save_to_Storege(e.target);
	} else {
		$(e.target).removeClass("fa-star");
		$(e.target).addClass("fa-star-o");
		remove_from_Storege(e.target);
	}
}

function get_Key_from_element(element) {
	var href = $(element).find('.link-s').attr('href');
	return href.split('docview/')[1]
}

function get_Star_from_element(element) {
	return $(element).find('.fa-star-o')[0];
}

function getKeyFromURL() {
	return location.pathname.split('docview/')[1].replace('#','')
}

function read_stored_elements() {
	if ( $('#list_flag').length == 0 ) {
		var key = getKeyFromURL();
		if ( localStorage.getItem('csms_'+key) ) {
			$('.fa-star-o').addClass("fa-star");
			$('.fa-star-o').removeClass("fa-star-o");
		}	
	} else {
		$('.list_box').each(function(i,element) {
			var key = get_Key_from_element(element);
			if ( localStorage.getItem('csms_'+key) ) {
				var star = get_Star_from_element(element)
				$(star).addClass("fa-star");
				$(star).removeClass("fa-star-o");
			}
		});			
	}
	read_font_size();
}

function startsWith(str, s) {
	return str.lastIndexOf(s, 0) === 0
};

function showStarred(e) {
	$('.main_container').remove();
	$('<div class="col-xs-12 main_container"><div id="stareed_content" class="row"></div></div>').insertAfter( ".white_bar" );
	$('#doc_container').remove();
	
	
	for ( var i = 0, len = localStorage.length; i < len; ++i ) {
		
		if ( startsWith( localStorage.key(i), 'csms_')) {
			$('#stareed_content').append(localStorage.getItem(localStorage.key(i)))
		}
	}
	$('i.fa-star-o, i.fa-star').bind('click', (function(e){clickStar(e)}));
	$('i.fa-envelope-o').bind('click', (function(e){mailSend(e)}));
	read_stored_elements();
	$('body').append('<div id="list_flag"></div>');
	font_size_hand();
	read_font_size();
	$('#subcaption').html(' - Show Starred');
}

function clearStarred(e) {
	for ( var key in localStorage ) {	
		if ( startsWith( key, 'csms_')) {
			localStorage.removeItem(key);
		}
	}
	$('i.fa-star, i.fa-star-o').addClass("fa-star-o");
}

function get_node_content_by_obj_1(obj) {
	return obj.parentNode.parentNode.parentNode.parentNode.outerHTML;
}

function get_Starred_Item_text(element) {
	var caption = myTrim($(element).find('.link-s').text());
	var link = location.origin + $(element).find('.link-s').attr('href').replace(' ','%20');
	var date = $(element).find('.fa-calendar').parent().text();	
	var re = myTrim($(element).find('p').text());
	return caption + '  |  ' + date + '  |  ' + link + '\n' + re; 
}

function get_signature() {
	var signature = '\n--\n';
	signature += 'Thank you for visiting CustomsMobile!\n';
	signature += '"The Only Customs Website You\'ll Ever Need"\n';
	signature += 'http://www.customsmobile.com';
	return signature
}

function mailStarred(e) {

	var a = e.target;
	$('.ghost').html('');
	for ( var i = 0, len = localStorage.length; i < len; ++i ) {
		
		if ( startsWith( localStorage.key(i), 'csms_')) {
			$('.ghost').append(localStorage.getItem(localStorage.key(i)))
		}
	}
	var body = '';
	$('.ghost .list_box').each(function(i,element) {
		body += get_Starred_Item_text(element);
		body += '\n===================================================================\n\n'
	});		
	
	$(a).attr("href", 'mailto:?subject=NOTICES from customsmobile.com&body='+encodeURIComponent(body+get_signature()));
}

function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}


function get_DocDescription(e) {
	var title = $( "b:contains('Title : ')" ).text().split('Title : ')[1];
	var description = $('pre').text().split(' ', 32).join(' ')+' ...';
	return myTrim(title) + '\n' + description
}

function mailSend(e) {
	if ( $('#list_flag').length == 0 ) {		
		var a = e.target.parentNode;	
		var caption = myTrim($('#caption').text());
		var link = location.origin + '/notices/docview/' + getKeyFromURL();
		var date = $("#date").text();	
		var re = get_DocDescription(e);
		var body = caption + '  |  ' + date + '  |  ' + link + '\n' + re + '\n'; 
		$(a).attr("href", 'mailto:?subject=NOTICES from customsmobile.com ('+caption+')&body='+encodeURIComponent(body+get_signature()));		
		
	} else {
		var a = e.target.parentNode;	
		var caption = myTrim($(a.parentNode.parentNode).find('.link-s').text());
		var link = location.origin + $(a.parentNode.parentNode).find('.link-s').attr('href').replace(' ','%20');
		var date = $(a.parentNode.parentNode).find('.fa-calendar').parent().text();	
		var re = myTrim($(a.parentNode.parentNode.parentNode).find('p').text());
		var body = caption + '  |  ' + date + '  |  ' + link + '\n' + re + '\n'; 

		$(a).attr("href", 'mailto:?subject=NOTICES from customsmobile.com ('+caption+')&body='+encodeURIComponent(body+get_signature()));		
	}
}


$(document).ready(function() {

	read_stored_elements()
    $('i.fa-star-o, i.fa-star').bind('click', (function(e){clickStar(e)}));
	$('i.fa-envelope-o').bind('click', (function(e){mailSend(e)}));
	$('.fa-eye').parent().bind('click', (function(e){showStarred(e)}));
	$('.fa-envelope').parent().bind('click', (function(e){mailStarred(e)}));
	$('.fa-trash-o').parent().bind('click', (function(e){clearStarred(e)}));

});

