<?php
$siteurl = 'https://www.customsmobile.com/static/';
$host = "localhost";
$user_db = "custopk8_mldva14";
$pass_db = "N<r\")%\,Qr24&3$*Y~p'2j6-->=!3uPs";
$db_c = "custopk8_cfrxml";
$db = mysql_connect($host,$user_db,$pass_db);
mysql_select_db($db_c,$db);
?>
<!DOCTYPE html>
<html lang="en">
<!-- Head BEGIN =========================================== -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--
<meta name="viewport" content="width=device-width, initial-scale=1.0">
Remmed out the above, since the below 2 lines prevent pinch-to-zoom, since we now have icons to +/- font size
-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="HandheldFriendly" content="true" />
<meta name="description" content="CustomsMobile is a U.S. Customs (CBP) focused website created to provide access to Rulings (CROSS), Notices (CSMS), HTSUS, Porrt information, and Federal Regulations through a free, fun, and easy to use mobiile format">
<meta name="author" content="Bill S." >
<link rel="shortcut icon" href="img/ico/fav-icon.png">
<title>CustomsMobile</title>
<!--[if IE 7]>
<link rel="stylesheet" href="css/font-awesome-ie7.min.css">
<![endif]-->
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>style.css" rel="stylesheet">
<style>
.bs{
border-left-color: #5BC0DE;
padding: 15px;
margin: 15px 0px;
border-width: 1px 1px 1px 5px;
border-style: solid;
border-color: #EEE;
-moz-border-top-colors: none;
-moz-border-right-colors: none;
-moz-border-bottom-colors: none;
-moz-border-left-colors: none;
border-image: none;
border-radius: 3px;
min-height:20px;
overflow:hidden;
culor:#777;
}
#pagina{
width:100%;
clear:both;
height:35px;
padding-top:40px;
padding-bottom:40px;
}
#previous{
width:25%;
float:left;
}
#next{
width:25%;
float:right;	
}
#center{
width:50%;
float:left;
}
#center li{
margin-left:5px;
margin-right:5px;
}
#center2{
width:50%;
float:left;
display:none;
}
@media screen and (max-width: 500px) {
#center{
display:none;
}
#center2{
display:block;
}
}
</style>
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="js/jquery.jfontsize-1.0.js"></script>
<!-- Start Google Analytics code -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-52746142-1', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics code -->


</head>

<!-- Head END =========================================== -->
<body class="body-push" style="zoom: 1;">
<!-- Header BEGIN ========================================= -->
<div class="col-xs-12 top_bar">
<div class="row">
<div class="col-xs-3">
<h1><b>REGULATIONS&nbsp;</b><b1 id="subcaption"></b1></h1>
</div>
<div class="col-xs-9 text-right">
<div class="font_size_new_plusminus_container">
<a class="jfontsize-button" id="jfontsize-m2"
style="cursor: pointer;"><div class="font_size_new_plusminus">A-</div></a>
<a class="jfontsize-button" id="jfontsize-p2"
style="cursor: pointer;"><div class="font_size_new_plusminus">A+</div></a>
</div>
<!-- The below <label> menu options are hidden through a CSS tag in /static/style.css -->
<label>
<select>
<option selected="">Select Language</option>
<option>Short Option</option>
<option>This Is A Longer Option</option>
</select>
</label>
<div id="menuToggle" class="">
<i class="fa fa-bars"></i>
</div>
<!-- Navigation BEGIN ========================================= -->
<nav class="nav-left" id="theMenu">
<div class="menu-wrap">
<i class="fa fa-times-circle menu-close"></i>
<ul>
<li><a href="/cross"> <i class="fa fa-search"></i> Rulings
</a></li>
<li><a href="/notices"> <i class="fa fa-file-text-o"></i>
Notices
</a></li>
<li><a href="/usitc"> <i class="fa fa-table"></i> HTSUS
</a></li>
<li><a href="/static/regs.php"> <i class="fa fa-tasks"></i>
Regulations
</a></li>
<li><a href="/ports"> <i class="fa fa-plane"></i> Ports
</a></li>
<li><a href="/about.html"> <i class="fa fa-user"></i> About
</a></li>
</ul>
<!-- Social Media Start -->
<a href="https://www.facebook.com/pages/CustomsMobile/559630777516593"><i class="fa fa-facebook-square facebook"></i></a>
<a href="https://twitter.com/CustomsMobile"><i class="fa fa-twitter twitter"></i></a>
<!-- Social Media End -->
</div>
</nav>

<!-- Navigation END =========================================== -->
</div>
</div>
</div>
<!-- Bootstrap core JavaScript ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
(function() {
// Menu settings
$('#menuToggle, .menu-close').on('click', function() {
$('#menuToggle').toggleClass('active');
$('body').toggleClass('body-push-toleft');
$('#theMenu').toggleClass('menu-open');
});
$('#menuToggleright, .menu-closeright').on('click', function() {
$('#menuToggleright').toggleClass('active');
$('body').toggleClass('body-push-toright');
$('#theMenuright').toggleClass('menu-open');
});
})(jQuery)
</script>

<!-- Header END =========================================== -->
<div id="content">


<div class="container">
<div class="row">
<!-- Header BEGIN ========================================= -->
<div class="col-xs-12 blue_bar">

<div class="row">
<div class="col-xs-9 form-inline search_bar">
<div class="row">
<form method="post" action="<?php echo $siteurl; ?>regs.php">
<div class="form-group col-xs-6">
<input class="form-control search_text" name="search" value="" placeholder="Search" type="search"> <span class="search_icon"><i class="fa fa-search"></i></span>
</div>
<?php
if(isset($_GET["chapter"])):
?>
<div class="form-group col-xs-3" style="margin-right:15px">
<div class="row">
<select name="ps2" class="form-control select_box">
<?php
if(isset($_GET["subpart"]) AND isset($_GET["section"])):
?>
<option value="SUBPART-<?php echo $_GET["subpart"]; ?>">Subpart</option>
<option value="PART-<?php echo $_GET["part"]; ?>">Part</option>
<option value="CHAPTER-<?php echo $_GET["chapter"]; ?>">Chapter</option>
<?php
elseif(isset($_GET["subpart"])):
?>
<option value="SUBPART-<?php echo $_GET["subpart"]; ?>">Subpart</option>
<option value="PART-<?php echo $_GET["part"]; ?>">Part</option>
<option value="CHAPTER-<?php echo $_GET["chapter"]; ?>">Chapter</option>
<?php
elseif(isset($_GET["part"]) AND empty($_GET["subpart"])):
?>
<option value="PART-<?php echo $_GET["part"]; ?>">Part</option>
<option value="CHAPTER-<?php echo $_GET["chapter"]; ?>">Chapter</option>
<?php
elseif(isset($_GET["part"]) AND empty($_GET["subpart"])):
?>
<option value="PART-<?php echo $_GET["part"]; ?>">Part</option>
<?php
else:
?>
<option value="CHAPTER-<?php echo $_GET["chapter"]; ?>">Chapter</option>
<?php
endif;
?>
<option value="all">Title 19</option>
</select>
</div>
</div>
<?php
endif;
?>
<div class="hidden">
<div class="form-group col-xs-2">
<div class="row">
<select name="ps" class="form-control select_box">
<option value="25">25</option>
<option value="50" selected>50</option>
<option value="100">100</option>
<option value="0">All results</option>
</select>
</div>
</div>
</div>
<div class="form-group col-xs-2 padding_zero">
<button name="s" type="submit" class="btn btn-default btn-black">GO</button>
</div>
</div>
</form>
</div>
<div class="col-xs-3 back_btn text-right">
<a style="cursor: pointer;" href="javascript: history.go(-1)"> <i class="fa fa-angle-left"></i> Back
</a>
</div>
</div>

</div>
<!-- Header END =========================================== -->
<!-- STATIC doc BEGIN ========================================= -->

<div class="col-xs-12 main_container padding-zero-tp">
<div class="row">
<div class="col-xs-12 list_box dark_color">
<div class="col-xs-12 abt padding_zero">
<div class="bs">
<?php
function get_last($id,$type)
{
$gg = mysql_query("SELECT id FROM info WHERE type='$type' AND id > '".$id."'") or die (mysql_error());
return mysql_result($gg,0,"id");
}
function get_url($id,$type)
{
$id = mysql_real_escape_string($id);
$get = mysql_query("SELECT id,title FROM info WHERE type='subpart' AND id < '".$id."' ORDER BY id DESC LIMIT 1") or die (mysql_error());	
$re = mysql_fetch_array($get);
$aw = explode(" - ",$re["title"]);
$get2 = mysql_query("SELECT id,title FROM info WHERE type='part' AND id < '".$id."' ORDER BY id DESC LIMIT 1") or die (mysql_error());	
$re2 = mysql_fetch_array($get2);
$aw2 = explode(" - ",$re2["title"]);
$get3 = mysql_query("SELECT id,title FROM info WHERE type='chapter' AND id < '".$id."' ORDER BY id DESC LIMIT 1") or die (mysql_error());	
$re3 = mysql_fetch_array($get3);
$get4 = mysql_query("SELECT id,title,content FROM info WHERE id = '".$id."'") or die (mysql_error());	
$re4 = mysql_fetch_array($get4);
$aw4 = explode("</SECTNO>",$re4["content"]);
$dw = explode("<SECTNO>",$aw4[0]);
if($type == "subpart"):
$div = explode("[",$aw[0]);
if(strlen($aw[0]) == 0):
return "none";
else:
return '<a href="?chapter='.$re3["id"].'&part='.$re2["id"].'&subpart='.$re["id"].'">'.$div[0].'</a>';
endif;
endif;
if($type == "part"):
$div = explode("[",$aw2[0]);
$div2 = explode("]",$re2["title"]);
return '<a href="?chapter='.$re3["id"].'&part='.$re2["id"].'">'.$div[0].' '.$div2[1].'</a>';
endif;
if($type == "chapter"):
$vb = explode(" - ",$re3["title"]);
return '<a href="?chapter='.$re3["id"].'">'.$vb[0].'</a>';
endif;
if($type == "section"):
return $dw[1];
endif;
}
function get_url2($id)
{
$id = mysql_real_escape_string($id);
$get = mysql_query("SELECT id,title FROM info WHERE type='subpart' AND id < '".$id."' ORDER BY id DESC LIMIT 1") or die (mysql_error());	
$re = mysql_fetch_array($get);
$get2 = mysql_query("SELECT id,title FROM info WHERE type='part' AND id < '".$id."' ORDER BY id DESC LIMIT 1") or die (mysql_error());	
$re2 = mysql_fetch_array($get2);
$get3 = mysql_query("SELECT id,title FROM info WHERE type='chapter' AND id < '".$id."' ORDER BY id DESC LIMIT 1") or die (mysql_error());	
$re3 = mysql_fetch_array($get3);
if($re["id"] == ""):
return '?chapter='.$re3["id"].'&part='.$re2["id"];
else:
return '?chapter='.$re3["id"].'&part='.$re2["id"].'&subpart='.$re["id"];
endif;
}
function get_title($id)
{
$id = mysql_real_escape_string($id);
$get = mysql_query("SELECT title, content FROM info WHERE id='".$id."'") or die (mysql_error());	
$re = mysql_fetch_array($get);
if(preg_match("/ - /i",$re["title"])):
$tit = explode(" - ",$re["title"]);
return $tit[0];
else:
if(preg_match("/SECTNO/i",$re["content"])):
$n = explode("</SECTNO>",$re["content"]);
$d = explode("<SECTNO>",$n[0]);
return $d[1]; 
else:
return $re["title"];
endif;
endif;
}
if(isset($_POST["search"]) OR isset($_GET["search"]) OR isset($_GET["s"])):
if(isset($_GET["search"])):
$search = mysql_real_escape_string($_GET["search"]);
$search = str_replace('\"','"',$search);
if(preg_match('/"/i',$search)):
$ex = 1;
$search = str_replace('"','',$search);
elseif(isset($_GET["ex"])):
$ex = 1;
endif;
elseif(isset($_GET["s"])):
$search = mysql_real_escape_string($_GET["s"]);
$search = str_replace('\"','"',$search);
if(preg_match('/"/i',$search)):
$ex = 1;
$search = str_replace('"','',$search);
elseif(isset($_GET["ex"])):
$ex = 1;
endif;
else:
$search = mysql_real_escape_string($_POST["search"]);
$search = str_replace('\"','"',$search);
if(preg_match('/"/i',$search)):
$ex = 1;
$search = str_replace('"','',$search);
elseif(isset($_GET["ex"])):
$ex = 1;
endif;
endif;
if(!empty($_GET["section"])):
if(get_url($_GET["section"],"subpart") == "none"):
echo '<a href="'.$siteurl.'regs.php">Title 19</a> › '.get_url($_GET["section"],"chapter").' › '.get_url($_GET["section"],"part").' › '.get_url($_GET["section"],"section");
else:
echo '<a href="'.$siteurl.'regs.php">Title 19</a> › '.get_url($_GET["section"],"chapter").' › '.get_url($_GET["section"],"part").' › '.get_url($_GET["section"],"subpart").' › '.get_url($_GET["section"],"section");
endif;
else:
$se = explode(" ",$search);
$wq = count($se);
if($wq == 1):
$res = "content LIKE '%".$se[0]."%'";
elseif($wq == 2):
$res = "content LIKE '%".$se[0]."%' OR content LIKE '%".$se[1]."%'";
elseif($wq == 3):
$res = "content LIKE '%".$se[0]."%' AND (content LIKE '%".$se[1]."%' OR content LIKE '%".$se[2]."%')";
elseif($wq == 4):
$res = "content LIKE '%".$se[0]."%' AND (content LIKE '%".$se[1]."%' OR content LIKE '%".$se[2]."%' OR content LIKE '%".$se[3]."%')";
elseif($wq == 5):
$res = "content LIKE '%".$se[0]."%' AND (content LIKE '%".$se[1]."%' OR content LIKE '%".$se[2]."%' OR content LIKE '%".$se[3]."%' OR content LIKE '%".$se[4]."%')";
elseif($wq == 6):
$res = "content LIKE '%".$se[0]."%' AND (content LIKE '%".$se[1]."%' OR content LIKE '%".$se[2]."%' OR content LIKE '%".$se[3]."%' OR content LIKE '%".$se[4]."%' OR content LIKE '%".$se[5]."%')";
elseif($wq == 7):
$res = "content LIKE '%".$se[0]."%' AND (content LIKE '%".$se[1]."%' OR content LIKE '%".$se[2]."%' OR content LIKE '%".$se[3]."%' OR content LIKE '%".$se[4]."%' OR content LIKE '%".$se[5]."%' OR content LIKE '%".$se[6]."%')";
endif;
if(isset($ex)):
$tot = mysql_query("SELECT * FROM `info` WHERE content LIKE '%".$search."%' AND type='SECTION'");
else:
$tot = mysql_query("SELECT * FROM `info` WHERE $res AND type='SECTION'");
endif;
$total = mysql_num_rows($tot);
echo $total.' results for <strong>'.strtolower($search).'</strong>';
endif;
else:
$parti = explode("[",get_title(@$_GET["part"]));
$gt = explode("[",get_title(@$_GET["subpart"]));
if(!empty($_GET["section"]) AND !empty($_GET["subpart"])):
echo '<a href="'.$siteurl.'regs.php">Title 19</a> › <a href="'.$siteurl.'regs.php?chapter='.$_GET["chapter"].'">'.get_title($_GET["chapter"]).'</a> › <a href="'.$siteurl.'regs.php?chapter='.$_GET["chapter"].'&part='.$_GET["part"].'">'.$parti[0].'</a> › <a href="'.$siteurl.'regs.php?chapter='.$_GET["chapter"].'&part='.$_GET["part"].'&subpart='.$_GET["subpart"].'">'.$gt[0].'</a> › '.get_title($_GET["section"]);
elseif(!empty($_GET["section"]) AND empty($_GET["subpart"])):
echo '<a href="'.$siteurl.'regs.php">Title 19</a> › <a href="'.$siteurl.'regs.php?chapter='.$_GET["chapter"].'">'.get_title($_GET["chapter"]).'</a> › <a href="'.$siteurl.'regs.php?chapter='.$_GET["chapter"].'&part='.$_GET["part"].'">'.$parti[0].'</a> › '.get_title($_GET["section"]);
elseif(!empty($_GET["subpart"]) AND empty($_GET["section"])):
echo '<a href="'.$siteurl.'regs.php">Title 19</a> › <a href="'.$siteurl.'regs.php?chapter='.$_GET["chapter"].'">'.get_title($_GET["chapter"]).'</a> › <a href="'.$siteurl.'regs.php?chapter='.$_GET["chapter"].'&part='.$_GET["part"].'">'.$parti[0].'</a> › '.$gt[0];
elseif(!empty($_GET["part"]) AND empty($_GET["subpart"])):
echo '<a href="'.$siteurl.'regs.php">Title 19</a> › <a href="'.$siteurl.'regs.php?chapter='.$_GET["chapter"].'">'.get_title($_GET["chapter"]).'</a> › '.$parti[0];
elseif(!empty($_GET["chapter"]) AND empty($_GET["part"])):
echo '<a href="'.$siteurl.'regs.php">Title 19</a> › '.get_title($_GET["chapter"]);
else:
?>
<strong>Title 19</strong> | Current through April 1, 2014 |
<?php
endif;
endif;
?>
</div>
<div class="col-xs-11 padding_zero">
<?php
if(isset($search)):
if(isset($_POST['ps2']) AND $_POST['ps2'] !== "all"):
$ga = mysql_real_escape_string($_POST['ps2']);
elseif(isset($_GET['pe'])):
$ga = mysql_real_escape_string($_GET['pe']);
endif;
$search = mysql_real_escape_string($search);
if(50 > 0 OR @$_GET['limit'] > 0):
$se = explode(" ",$search);
$wq = count($se);
if($wq == 1):
$res = "content LIKE '%".$se[0]."%'";
elseif($wq == 2):
$res = "content LIKE '%".$se[0]."%' AND (content LIKE '%".$se[1]."%')";
elseif($wq == 3):
$res = "content LIKE '%".$se[0]."%' AND (content LIKE '%".$se[1]."%' OR content LIKE '%".$se[2]."%')";
elseif($wq == 4):
$res = "content LIKE '%".$se[0]."%' AND (content LIKE '%".$se[1]."%' OR content LIKE '%".$se[2]."%' OR content LIKE '%".$se[3]."%')";
elseif($wq == 5):
$res = "content LIKE '%".$se[0]."%' AND (content LIKE '%".$se[1]."%' OR content LIKE '%".$se[2]."%' OR content LIKE '%".$se[3]."%' OR content LIKE '%".$se[4]."%')";
elseif($wq == 6):
$res = "content LIKE '%".$se[0]."%' AND (content LIKE '%".$se[1]."%' OR content LIKE '%".$se[2]."%' OR content LIKE '%".$se[3]."%' OR content LIKE '%".$se[4]."%' OR content LIKE '%".$se[5]."%')";
elseif($wq == 7):
$res = "content LIKE '%".$se[0]."%' AND (content LIKE '%".$se[1]."%' OR content LIKE '%".$se[2]."%' OR content LIKE '%".$se[3]."%' OR content LIKE '%".$se[4]."%' OR content LIKE '%".$se[5]."%' OR content LIKE '%".$se[6]."%')";
endif;
if(isset($ga)):
$bh = explode("-",$ga);
$l_i = $bh[1];
$last = get_last($bh[1],$bh[0]);
if(isset($ex)):
$tot = mysql_query("SELECT * FROM `info` WHERE content LIKE '%".$search."%' AND id > '".$l_i."' AND id < '".$last."' AND type='SECTION'");
else:
$tot = mysql_query("SELECT * FROM `info` WHERE $res AND id > '".$l_i."' AND id < '".$last."' AND type='SECTION'");
endif;
else:
if(isset($ex)):
$tot = mysql_query("SELECT * FROM `info` WHERE content LIKE '%".$search."%' AND type='SECTION'");
else:
$tot = mysql_query("SELECT * FROM `info` WHERE $res AND type='SECTION'");
endif;
endif;
$total = mysql_num_rows($tot);
if(isset($_GET['limit'])):
$num = mysql_real_escape_string($_GET['limit']);
else:
$num = 50;
endif;
if(empty($_GET["page"]) OR $_GET["page"] == 1):
$page = 1;
$d = 0;
else:
$page = $_GET["page"];
$d = $page * $num - 50;
endif;
if(isset($ga)):
$bh = explode("-",$ga);
$l_i = $bh[1];
$last = get_last($bh[1],$bh[0]);
if(isset($ex)):
$get = mysql_query("SELECT * FROM `info` WHERE content LIKE '%".$search."%' AND type='SECTION' AND id > '".$l_i."' AND id < '".$last."' ORDER BY content LIMIT $d,".$num) or die (mysql_error());
else:
$get = mysql_query("SELECT * FROM `info` WHERE $res AND type='SECTION' AND id > '".$l_i."' AND id < '".$last."' ORDER BY content LIMIT $d,".$num) or die (mysql_error());
endif;
else:
if(isset($ex)):
$get = mysql_query("SELECT * FROM `info` WHERE content LIKE '%".$search."%' AND type='SECTION' ORDER BY content LIMIT $d,".$num) or die (mysql_error());
else:
$get = mysql_query("SELECT * FROM `info` WHERE $res AND type='SECTION' ORDER BY content LIMIT $d,".$num) or die (mysql_error());
endif;
endif;
else:
if(isset($ex)):
$get = mysql_query("SELECT * FROM `info` WHERE content LIKE '%".$search."%' AND type='SECTION' ORDER BY content") or die (mysql_error());
else:
$get = mysql_query("SELECT * FROM `info` WHERE $res AND type='SECTION' ORDER BY content") or die (mysql_error());
endif;
endif;
$csec = mysql_num_rows($get);
$c = 0;
$dz = "";
$part = "";
$ch = "";
if(isset($_GET["section"]) AND isset($_GET["search"])):
$id = mysql_real_escape_string($_GET['section']);
$seb = mysql_query("SELECT * FROM `info` WHERE id='$id'");
$con = explode("</SUBJECT>",mysql_result($seb,0,"content"));
$search = strtolower($search);
$se = explode(" ",$search);
$wq = count($se);
if($wq == 1):
$replace = array($se[0],ucfirst($se[0]));
$to = array('<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[0].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[0]).'</strong>');
elseif($wq == 2):
$replace = array($se[0],ucfirst($se[0]),$se[1],ucfirst($se[1]));
$to = array('<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[0].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[0]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[1].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[1]).'</strong>');
elseif($wq == 3):
$replace = array($se[0],ucfirst($se[0]),$se[1],ucfirst($se[1]),$se[2],ucfirst($se[2]));
$to = array('<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[0].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[0]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[1].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[1]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[2].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[2]).'</strong>');
elseif($wq == 4):
$replace = array($se[0],ucfirst($se[0]),$se[1],ucfirst($se[1]),$se[2],ucfirst($se[2]),$se[3],ucfirst($se[3]));
$to = array('<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[0].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[0]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[1].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[1]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[2].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[2]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[3].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[3]).'</strong>');
elseif($wq == 5):
$replace = array($se[0],ucfirst($se[0]),$se[1],ucfirst($se[1]),$se[2],ucfirst($se[2]),$se[3],ucfirst($se[3]),$se[4],ucfirst($se[4]));
$to = array('<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[0].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[0]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[1].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[1]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[2].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[2]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[3].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[3]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[4].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[4]).'</strong>');
elseif($wq == 6):
$replace = array($se[0],ucfirst($se[0]),$se[1],ucfirst($se[1]),$se[2],ucfirst($se[2]),$se[3],ucfirst($se[3]),$se[4],ucfirst($se[4]),$se[5],ucfirst($se[5]));
$to = array('<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[0].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[0]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[1].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[1]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[2].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[2]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[3].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[3]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[4].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[4]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[5].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[5]).'</strong>');
elseif($wq == 7):
$replace = array($se[0],ucfirst($se[0]),$se[1],ucfirst($se[1]),$se[2],ucfirst($se[2]),$se[3],ucfirst($se[3]),$se[4],ucfirst($se[4]),$se[5],ucfirst($se[5]),$se[6],ucfirst($se[6]));
$to = array('<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[0].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[0]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[1].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[1]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[2].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[2]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[3].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[3]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[4].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[4]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[5].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[5]).'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.$se[6].'</strong>','<strong style="background-color: yellow; padding-left:0px; padding-right:0px">'.ucfirst($se[6]).'</strong>');
endif;
echo '<div style="font-size: 11px; margin-left:20px; font-size: 11px; undefined"><p><strong>'.mysql_result($seb,0,"title").'</strong></p>';
echo str_replace($replace,$to,$con[1]);
echo '</div>';
endif;
while($c < $csec):
$id3 = mysql_result($get,$c,"id");
$content = mysql_result($get,$c,"content");
$con = explode("</SUBJECT>",$content);
$g = str_replace("<SUBJECT>","",$con[0]);
$g = str_replace("</SECTNO>"," - ",$g);
$g5 = str_replace("<SECTNO>","",$g);
$g = explode(" - ",$g5);
if(empty($_GET["section"])):
if(get_url($id3,"subpart") == "none"):
if($dz !== get_url($id3,"part")):
if($ch !== get_url($id3,"chapter")):
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:20px; undefined">'.get_url($id3,"chapter").'</li></ul>';
endif;
$ch = get_url($id3,"chapter");
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:20px; undefined"><a href="?search='.$search.'&section='.$id3.'">'.get_url($id3,"part").'</a></li></ul>';
if(preg_match('/Reserved/i',@$g[1])):
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:30px; undefined"> › '.$g[0].' '.$g[1].'</li></ul>';
else:
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:30px; undefined"> › <a href="?search='.$search.'&section='.$id3.'">'.@$g[0].' '.@$g[1].'</a></li></ul>';
endif;
$dz = get_url($id3,"part");
else:
if(preg_match('/Reserved/i',@$g[1])):
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:30px; undefined"> › '.@$g[0].' '.@$g[1].'</li></ul>';
else:
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:30px; undefined"> › <a href="?search='.$search.'&section='.$id3.'">'.@$g[0].' '.@$g[1].'</a></li></ul>';
endif;
endif;
else:
if($dz !== get_url($id3,"subpart")):
if($part !== get_url($id3,"part")):
if($ch !== get_url($id3,"chapter")):
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:20px; undefined">'.get_url($id3,"chapter").'</li></ul>';
$ch = get_url($id3,"chapter");
endif;
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:20px; undefined;"><a href="?search='.$search.'&section='.$id3.'">'.get_url($id3,"part").'</a></li></ul>';
$part = get_url($id3,"part");
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:20px;"><a href="?search='.$search.'&section='.$id3.'">'.get_url($id3,"subpart").'</a></li></ul>';
else:
$part = get_url($id3,"part");
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:20px;"><a href="?search='.$search.'&section='.$id3.'">'.get_url($id3,"subpart").'</a></li></ul>';
endif;
if(preg_match('/Reserved/i',$g[1])):
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:30px; undefined"> › '.$g[0].' '.$g[1].'</li></ul>';
else:
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:30px; undefined"> › <a href="?search='.$search.'&section='.$id3.'">'.$g[0].' '.$g[1].'</a></li></ul>';
endif;
$dz = get_url($id3,"subpart");
else:
if(preg_match('/Reserved/i',$g[1])):
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:30px; undefined"> › '.$g[0].' '.$g[1].'</li></ul>';
else:
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:30px; undefined"> › <a href="?search='.$search.'&section='.$id3.'">'.$g[0].' '.$g[1].'</a></li></ul>';
endif;
endif;
endif;
endif;
$c++;
endwhile;
else:
$get_list = mysql_query("SELECT * FROM `info` WHERE `type`='CHAPTER'");
$total = mysql_num_rows($get_list);
$chap = 0;
while($chap < $total):
$id = mysql_result($get_list,$chap,"id");
if(empty($_GET["chapter"])):
if(mysql_result($get_list,$chap,"title") !== ""):
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:20px; undefined"><a href="?chapter='.$id.'">'.mysql_result($get_list,$chap,"title").'</a></li></ul>';
endif;
endif;
$chap++;
if($chap != $total):
if(isset($_GET["chapter"]) AND $_GET["chapter"] == $id):
$idi = mysql_result($get_list,$chap,"id");
$pa = mysql_query("SELECT * FROM `info` WHERE `type`='PART' AND `id` > '".$id."' AND `id` < '".$idi."'");
$total5 = mysql_num_rows($pa);
$p = 0;
while($p < $total5):
$id22 = mysql_result($pa,$p,"id");
if(empty($_GET["part"])):
if(preg_match("/RESERVED/i",mysql_result($pa,$p,"title"))):
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:20px; undefined">'.mysql_result($pa,$p,"title").'</li></ul>';
else:
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:20px; undefined"><a href="?chapter='.$id.'&part='.$id22.'">'.mysql_result($pa,$p,"title").'</a></li></ul>';
endif;
endif;
$p++;
if(isset($_GET["part"]) AND $_GET["part"] == $id22):
$idi11 = @mysql_result($pa,$p,"id");
if($idi11 == ""):
$idi11 = $idi;
endif;
$sub = mysql_query("SELECT * FROM `info` WHERE `type`='SUBPART' AND `id` > '".$id22."' AND `id` < '".$idi11."'");
$total2 = mysql_num_rows($sub);
$q = 0;
if($total2 == 0):
$sec = mysql_query("SELECT * FROM `info` WHERE `type`='section' AND `id` > '".$id22."' AND `id` < '".$idi11."'");
$csec = mysql_num_rows($sec);
$c = 0;
while($c < $csec):
$id3 = mysql_result($sec,$c,"id");
$content = mysql_result($sec,$c,"content");
$con = explode("</SUBJECT>",$content);
$g = str_replace("<SUBJECT>","",$con[0]);
$g = str_replace("</SECTNO>"," - ",$g);
$g = str_replace("<SECTNO>","",$g);

if(isset($_GET["section"]) AND $_GET["section"] == $id3):
echo '<div style="font-size: 11px; margin-left:20px;"><p> <b>'.$g.'</b></p>';
echo ''.@$con[1].'</div>';
else:
if(empty($_GET["section"])):
if(!preg_match('/Reserved/i',$g)):
if($g !== ""):
echo '<ul class="list-inline">
<li style="font-size: 11px;  margin-left:20px; undefined"><a href="?chapter='.$id.'&part='.$id22.'&section='.$id3.'">'.$g.'</a></li></ul>';
endif;
else:
echo '<ul class="list-inline">
<li style="font-size: 11px;  margin-left:20px; undefined">'.$g.'</li></ul>';
endif;
endif;
endif;
$c++;
endwhile;
endif;
while($q < $total2):
$id2 = mysql_result($sub,$q,"id");
if(empty($_GET["subpart"])):
if(preg_match('/Reserved/i',mysql_result($sub,$q,"title"))):
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:20px; undefined">'.mysql_result($sub,$q,"title").'</li></ul>';
else:
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:20px; undefined"><a href="?chapter='.$id.'&part='.$id22.'&subpart='.$id2.'">'.mysql_result($sub,$q,"title").'</a></li></ul>';
endif;
endif;
$q++;
if(isset($_GET["subpart"]) AND $_GET["subpart"] == $id2):
$idi2 = @mysql_result($sub,$q,"id");
if($idi2 == ""):
$idi2 = $idi;
endif;
$sec = mysql_query("SELECT * FROM `info` WHERE `type`='section' AND `id` > '".$id2."' AND `id` < '".$idi2."'");
$csec = mysql_num_rows($sec);
$c = 0;
while($c < $csec):
$id3 = mysql_result($sec,$c,"id");
$content = mysql_result($sec,$c,"content");
$con = explode("</SUBJECT>",$content);
$g = str_replace("<SUBJECT>","",$con[0]);
$g = str_replace("</SECTNO>"," - ",$g);
$g = str_replace("<SECTNO>","",$g);
if(isset($_GET["section"]) AND $_GET["section"] == $id3):
echo '<div style="font-size: 11px; margin-left:20px; undefined"><p> <b>'.$g.'</b></p>';
echo ''.@$con[1].'</div>';
else:
if(empty($_GET["section"])):
if(!preg_match('/Reserved/i',$g)):
if($g !== ""):
echo '<ul class="list-inline">
<li style="font-size: 11px;  margin-left:20px; undefined"><a href="?chapter='.$id.'&part='.$id22.'&subpart='.$id2.'&section='.$id3.'">'.$g.'</a></li></ul>';
endif;
else:
echo '<ul class="list-inline">
<li style="font-size: 11px;  margin-left:20px; undefined">'.$g.'</li></ul>';
endif;
endif;
endif;
$c++;
endwhile;
endif;
endwhile;
endif;
endwhile;
endif;
else:
if(isset($_GET["chapter"]) AND $_GET["chapter"] == $id):
$idi = @mysql_result($get_list,$chap,"id");
$pa = mysql_query("SELECT * FROM `info` WHERE `type`='PART' AND `id` > '".$id."'");
$total5 = mysql_num_rows($pa);
$p = 0;
while($p < $total5):
$id22 = mysql_result($pa,$p,"id");
if(empty($_GET["part"])):
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:20px; undefined"><a href="?chapter='.$id.'&part='.$id22.'">'.mysql_result($pa,$p,"title").'</a></li></ul>';
endif;
$p++;
if(isset($_GET["part"]) AND $_GET["part"] == $id22):
$idi11 = @mysql_result($pa,$p,"id");
$sub = mysql_query("SELECT * FROM `info` WHERE `type`='SUBPART' AND `id` > '".$id22."'");
$total2 = mysql_num_rows($sub);
if($total2 == 0):
$sec = mysql_query("SELECT * FROM `info` WHERE `type`='section' AND `id` > '".$id22."' AND `id` < '".$idi11."'");
$csec = mysql_num_rows($sec);
$c = 0;
while($c < $csec):
$id3 = mysql_result($sec,$c,"id");
$content = mysql_result($sec,$c,"content");
$con = explode("</SUBJECT>",$content);
$g = str_replace("<SUBJECT>","",$con[0]);
$g = str_replace("</SECTNO>"," - ",$g);
$g = str_replace("<SECTNO>","",$g);
if(isset($_GET["section"]) AND $_GET["section"] == $id3):
echo '<div style="font-size: 11px; margin-left:20px; undefined"><p> <b>'.$g.'</b></p>';
echo ''.@$con[1].'</div>';
else:
if(empty($_GET["section"])):
if(!preg_match('/Reserved/i',$g)):
echo '<ul class="list-inline">
<li style="font-size: 11px;  margin-left:20px; undefined"><a href="?chapter='.$id.'&part='.$id22.'&section='.$id3.'">'.$g.'</a></li></ul>';
endif;
endif;
endif;
$c++;
endwhile;
endif;
$q = 0;
while($q < $total2):
$id2 = mysql_result($sub,$q,"id");
if(empty($_GET["subpart"])):
echo '<ul class="list-inline">
<li style="font-size: 11px; margin-left:20px; undefined"><a href="?chapter='.$id.'&part='.$id22.'&subpart='.$id2.'">'.mysql_result($sub,$q,"title").'</a></li></ul>';
endif;
$q++;
if(isset($_GET["subpart"]) AND $_GET["subpart"] == $id2):
$idi2 = @mysql_result($sub,$q,"id");
$sec = mysql_query("SELECT * FROM `info` WHERE `type`='section' AND `id` > '".$id2."' AND `id` < '".$idi2."'");
$csec = mysql_num_rows($sec);
$c = 0;
while($c < $csec):
$id3 = mysql_result($sec,$c,"id");
$content = mysql_result($sec,$c,"content");
$con = explode("</SUBJECT>",$content);
$g = str_replace("<SUBJECT>","",$con[0]);
$g = str_replace("</SECTNO>"," - ",$g);
$g = str_replace("<SECTNO>","",$g);
if(isset($_GET["section"]) AND $_GET["section"] == $id3):
echo '<div style="font-size: 11px; margin-left:20px; undefined"><p> <b>'.$g.'</b></p>';
echo $con[1].'</div>';
else:
if(empty($_GET["section"])):
if(!preg_match('/[Reserved]/i',$g)):
echo '<ul class="list-inline">
<li style="font-size: 11px;  margin-left:20px; undefined"><a href="?chapter='.$id.'&part='.$id22.'&subpart='.$id2.'&section='.$id3.'">'.$g.'</a></li></ul>';
endif;
endif;
endif;
$c++;
endwhile;
endif;
endwhile;
endif;
endwhile;
endif;
endif;
endwhile;
endif;
?>
<div id="pagina" style="margin-top:30px">
<?php
if(isset($_GET["section"]) AND !isset($_GET["search"])):
$section = mysql_real_escape_string($_GET["section"]);
$min2 = $section - 1;
$max2 = $section + 1;
$ge_h = mysql_query("SELECT id,title,type FROM `info` WHERE `id` >= '".$min2."' AND `id` <= '".$max2."'");
$min = mysql_result($ge_h,0,"type");
$max = mysql_result($ge_h,2,"type");
$mi = mysql_result($ge_h,0,"title");
if(strlen($mi) < 1):
$mi = "non";
endif;
$ma = mysql_result($ge_h,2,"title");
if(strlen($ma) < 1):
$ma = "non";
endif;
$chapter = mysql_real_escape_string($_GET["chapter"]);
$part = mysql_real_escape_string($_GET["part"]);
$subpart = mysql_real_escape_string(@$_GET["subpart"]);
if($min == 'SECTION' AND $mi !== "non"):
if(!empty($_GET["subpart"])):
?>
<div id="previous" align="left"><a href="<?php echo $siteurl; ?>regs.php?chapter=<?php echo $chapter; ?>&part=<?php echo $part; ?>&subpart=<?php echo $subpart; ?>&section=<?php echo $min2; ?>">&#10096; previous</a></div>
<?php
else:
?>
<div id="previous" align="left"><a href="<?php echo $siteurl; ?>regs.php?chapter=<?php echo $chapter; ?>&part=<?php echo $part; ?>&section=<?php echo $min2; ?>">&#10096; previous</a></div>
<?php
endif;
endif;
if($max == 'SECTION' AND $ma !== "non"):
if(!empty($_GET["subpart"])):
?>
<div id="next" align="right"><a href="<?php echo $siteurl; ?>regs.php?chapter=<?php echo $chapter; ?>&part=<?php echo $part; ?>&subpart=<?php echo $subpart; ?>&section=<?php echo $max2; ?>">next &#10097;</a></div>
<?php
else:
?>
<div id="next" align="right"><a href="<?php echo $siteurl; ?>regs.php?chapter=<?php echo $chapter; ?>&part=<?php echo $part; ?>&section=<?php echo $max2; ?>">next &#10097; </a></div>
<?php
endif;
endif;
elseif(isset($_GET["search"])):
$section = mysql_real_escape_string($_GET["section"]);
$min2 = $section - 1;
$max2 = $section + 1;
$ge_h = mysql_query("SELECT id,title,type FROM `info` WHERE `id` >= '".$min2."' AND `id` <= '".$max2."'");
$min = mysql_result($ge_h,0,"type");
$max = mysql_result($ge_h,2,"type");
$mi = mysql_result($ge_h,0,"title");
if(strlen($mi) < 1):
$mi = "non";
endif;
$ma = mysql_result($ge_h,2,"title");
if(strlen($ma) < 1):
$ma = "non";
endif;
if($min == 'SECTION' AND $mi !== "non"):
if(!empty($_GET["subpart"])):
?>
<div id="previous" align="left"><a href="<?php echo get_url2($min2); ?>&section=<?php echo $min2; ?>">&#10096; previous</a></div>
<?php
else:
?>
<div id="previous" align="left"><a href="<?php echo get_url2($min2); ?>&section=<?php echo $min2; ?>">&#10096; previous</a></div>
<?php
endif;
endif;
if($max == 'SECTION' AND $ma !== "non"):
if(!empty($_GET["subpart"])):
?>
<div id="next" align="right"><a href="<?php echo get_url2($max2); ?>&section=<?php echo $max2; ?>">next &#10097;</a></div>
<?php
else:
?>
<div id="next" align="right"><a href="<?php echo get_url2($max2); ?>&section=<?php echo $max2; ?>">next &#10097; </a></div>
<?php
endif;
endif;
endif;
if(isset($_POST["search"]) OR isset($_GET["s"])):
if($page > 1):
$pa = $page - 1;
if(isset($ga)):
if(isset($ex)):
?>
<div id="previous" align="left"><?php echo '<a href="'.$siteurl.'regs.php?s='.$search.'&page='.$pa.'&limit='.$num.'&pe='.$ga.'&ex=1">&#10096; previous</a>'; ?></div>
<?php
else:
?>
<div id="previous" align="left"><?php echo '<a href="'.$siteurl.'regs.php?s='.$search.'&page='.$pa.'&limit='.$num.'&pe='.$ga.'">&#10096; previous</a>'; ?></div>
<?php
endif;
else:
if(isset($ex)):
?>
<div id="previous" align="left"><?php echo '<a href="'.$siteurl.'regs.php?s='.$search.'&page='.$pa.'&limit='.$num.'&ex=1">&#10096; previous</a>'; ?></div>
<?php
else:
?>
<div id="previous" align="left"><?php echo '<a href="'.$siteurl.'regs.php?s='.$search.'&page='.$pa.'&limit='.$num.'">&#10096; previous</a>'; ?></div>
<?php
endif;
endif;
else:
?>
<div id="previous" align="left">&#10096; previous</div>
<?php
endif;
?>
<div id="center2" align="center">
<?php echo $page; ?>/<?php echo ceil($total/$num); ?>
</div>
<div id="center" align="center">
<ul>
<?php
$w = $total/50;
$d = $page;
$fi = 2;
if($page < 3):
$fd = 1;
if($page == 2):
$fi = 1;
else:
$fi = 2;
endif;
else:
$fd = $page - 2;
endif;
while($fd <= ceil($w)):
if($fd == $page):
echo '<li>'.$fd.'</li>';
else:
if(isset($ga)):
if(isset($ex)):
echo '<li><a href="'.$siteurl.'regs.php?s='.$search.'&page='.$fd.'&limit='.$num.'&pe='.$ga.'&ex=1">'.$fd.'</a></li>';
else:
echo '<li><a href="'.$siteurl.'regs.php?s='.$search.'&page='.$fd.'&limit='.$num.'&pe='.$ga.'">'.$fd.'</a></li>';
endif;
else:
if(isset($ex)):
echo '<li><a href="'.$siteurl.'regs.php?s='.$search.'&page='.$fd.'&limit='.$num.'&ex=1">'.$fd.'</a></li>';
else:
echo '<li><a href="'.$siteurl.'regs.php?s='.$search.'&page='.$fd.'&limit='.$num.'">'.$fd.'</a></li>';
endif;
endif;
endif;
if($fd > $d + 1):
if($fd < ceil($w)):
$w2 = ceil($w);
if(isset($ga)):
if(isset($ex)):
echo '<li>...</li><li><a href="'.$siteurl.'regs.php?s='.$search.'&page='.$w2.'&limit='.$num.'&pe='.$ga.'&ex=1">'.$w2.'</a></li>';
else:
echo '<li>...</li><li><a href="'.$siteurl.'regs.php?s='.$search.'&page='.$w2.'&limit='.$num.'&pe='.$ga.'">'.$w2.'</a></li>';
endif;
else:
if(isset($ex)):
echo '<li>...</li><li><a href="'.$siteurl.'regs.php?s='.$search.'&page='.$w2.'&limit='.$num.'&ex=1">'.$w2.'</a></li>';
else:
echo '<li>...</li><li><a href="'.$siteurl.'regs.php?s='.$search.'&page='.$w2.'&limit='.$num.'">'.$w2.'</a></li>';
endif;
endif;
endif;
break;
else:
$fd++;
endif;
endwhile;
?>
</ul>
</div>
<?php
if($page <  ceil($w)):
$pa = $page + 1;
if(isset($ga)):
if(isset($ex)):
?>
<div id="next" align="right"><?php echo '<a href="'.$siteurl.'regs.php?s='.$search.'&page='.$pa.'&limit='.$num.'&pe='.$ga.'&ex=1">next &#10097;</a>'; ?></div>
<?php
else:
?>
<div id="next" align="right"><?php echo '<a href="'.$siteurl.'regs.php?s='.$search.'&page='.$pa.'&limit='.$num.'&pe='.$ga.'">next &#10097;</a>'; ?></div>
<?php
endif;
else:
if(isset($ex)):
?>
<div id="next" align="right"><?php echo '<a href="'.$siteurl.'regs.php?s='.$search.'&page='.$pa.'&limit='.$num.'&ex=1">next &#10097;</a>'; ?></div>
<?php
else:
?>
<div id="next" align="right"><?php echo '<a href="'.$siteurl.'regs.php?s='.$search.'&page='.$pa.'&limit='.$num.'">next &#10097;</a>'; ?></div>
<?php
endif;
endif;
else:
?>
<div id="next" align="right">next &#10097;</div>
<?php
endif;
endif;
?>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- STATIC doc END =========================================== -->
</div>
</div>
<!-- FONT RESIZE FUNCTION -->
<div style="display:none" id="font_size_attributes">.abt,.list_box li,p</div> <!-- DO NOT TOUCH -->

</div>
<!-- Footer BEGIN ========================================= -->
<!-- <footer id="footer">
<div id="footerContent">
<h5></h5>
</div>
</footer> -->
<div class="container">
<div class="row">
<!--Start disclaimer & privacy links. Created with div -->
<div class="footer-link-a">
&copy; 2014 CustomsMobile | <a href="/disclaimer.html">Disclaimer</a> | <a href="/privacy.html">Privacy</a>
</div>
<!-- End disclaimer & privacy links. Created with div -->
<!-- Start advertisement placeholder
<div class="col-xs-12">
<div class="row">
<div class="col-xs-12 adv_container">
<img src="/static/img/adv.png" class="img-responsive" alt="" />
</div>
</div>
</div>
End advertisement placeholder -->
</div>
</div>

<!-- Footer END =========================================== -->
<script type="text/javascript" language="javascript" src="js/jquery.jfontsize-1.0.js"></script>
<script type="text/javascript" language="javascript">
font_size_hand();
read_font_size();
$('.jfontsize-button').bind('click', (function(e){write_font_size()}));
</script>
</body>
</html>

