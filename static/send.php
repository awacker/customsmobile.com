<!DOCTYPE html>
<html lang="en">
<!-- Head BEGIN =========================================== -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--
<meta name="viewport" content="width=device-width, initial-scale=1.0">
Remmed out the above, since the below 2 lines prevent pinch-to-zoom, since we now have icons to +/- font size
-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="HandheldFriendly" content="true" />
<meta name="description" content="CustomsMobile is a U.S. Customs (CBP) focused website created to provide access to Rulings (CROSS), Notices (CSMS), HTSUS, Porrt information, and Federal Regulations through a free, fun, and easy to use mobiile format">
<meta name="author" content="">
<link rel="shortcut icon" href="/static/img/ico/fav-icon.png">
<title>CustomsMobile</title>
<!--[if IE 7]>
<link rel="stylesheet" href="css/font-awesome-ie7.min.css">
<![endif]-->
<link href="/static/css/font-awesome.min.css" rel="stylesheet">
<link href="/static/style.css" rel="stylesheet">
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="/static/js/jquery.jfontsize-1.0.js"></script>
<!-- Start Google Analytics code -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-52746142-1', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics code -->


</head>

<!-- Head END =========================================== -->
<body class="body-push" style="zoom: 1;">
<!-- Header BEGIN ========================================= -->
<div class="col-xs-12 top_bar">
<div class="row">
<div class="col-xs-3">
<h1><b>CONTACT</b><b1 id="subcaption"></b1></h1>
</div>
<div class="col-xs-9 text-right">
<div class="font_size_new_plusminus_container">
<a class="jfontsize-button" id="jfontsize-m2"
style="cursor: pointer;"><div class="font_size_new_plusminus">A-</div></a>
<a class="jfontsize-button" id="jfontsize-p2"
style="cursor: pointer;"><div class="font_size_new_plusminus">A+</div></a>
</div>
<!-- The below <label> menu options are hidden through a CSS tag in /static/style.css -->
<label>
<select>
<option selected="">Select Language</option>
<option>Short Option</option>
<option>This Is A Longer Option</option>
</select>
</label>
<div id="menuToggle" class="">
<i class="fa fa-bars"></i>
</div>
<!-- Navigation BEGIN ========================================= -->
<nav class="nav-left" id="theMenu">
<div class="menu-wrap">
<i class="fa fa-times-circle menu-close"></i>
<ul>
<li><a href="/cross"> <i class="fa fa-search"></i> Rulings
</a></li>
<li><a href="/notices"> <i class="fa fa-file-text-o"></i>
Notices
</a></li>
<li><a href="/usitc"> <i class="fa fa-table"></i> HTSUS
</a></li>
<li><a href="/regs.html"> <i class="fa fa-tasks"></i>
Regulations
</a></li>
<li><a href="/ports"> <i class="fa fa-plane"></i> Ports
</a></li>
<li><a href="/about.html"> <i class="fa fa-user"></i> About
</a></li>
</ul>
</div>
</nav>

<!-- Navigation END =========================================== -->
</div>
</div>
</div>
<!-- Bootstrap core JavaScript ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"
type="text/javascript"></script>
<script src="/static/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">
(function() {
// Menu settings
$('#menuToggle, .menu-close').on('click', function() {
$('#menuToggle').toggleClass('active');
$('body').toggleClass('body-push-toleft');
$('#theMenu').toggleClass('menu-open');
});
$('#menuToggleright, .menu-closeright').on('click', function() {
$('#menuToggleright').toggleClass('active');
$('body').toggleClass('body-push-toright');
$('#theMenuright').toggleClass('menu-open');
});
})(jQuery)
</script>

<!-- Header END =========================================== -->
<div id="content">


<div class="container">
<div class="row">
<!-- Header BEGIN ========================================= -->
<div class="col-xs-12 blue_bar">
<div class="row">
<div class="col-xs-9 form-inline search_bar"></div>
<div class="col-xs-3 back_btn text-right">
<a id="h_path" style='cursor: pointer;' href='/'> <i class="fa fa-angle-left"></i> Back </a>
</div>
</div>
</div>
<!-- Header END =========================================== -->
<!-- STATIC doc BEGIN ========================================= -->

<div class="col-xs-12 main_container padding-zero-tp">
<div class="row">
<div class="col-xs-12 list_box dark_color">
<div class="col-xs-12 abt padding_zero">

<p>
<?php
  $admin_email = "support@customsmobile.com";
  $subject = "Contact Form";
  $name = $_REQUEST['contactname'];
  $email = $_REQUEST['contactemail'];
  $number = $_REQUEST['contactnumber'];
  $website = $_REQUEST['contactwebsite'];
  $comment = $_REQUEST['contactmessage'];
	$message .= "Name: ".$name."\n";
	$message .= "Email: ".$email."\n";
        $message .= "Phone: ".$number."\n";
        $message .= "Website: ".$website."\n";
        $message .= "Comment: ".$comment."\n";

if($_POST['contactemail2'] != '') {
    echo "<p>Það kom upp vandamál við eyðublaðinu; sjálfvirk ruslpóst grípari læst uppgjöf þinni. Vinsamlegast farðu og reyndu aftur. Við biðjumst velvirðingar á ó þægindunum.</p>";
echo "<p>Hint: Google Translate, Icelandic</p>";

}

  elseif (empty($email) || empty($name) || empty($comment)) {
//header("refresh: 15; url=/about.html");
//echo "<script>";
//echo "setTimeout(function ()";
//echo "{";
//echo "window.location.href = '/about.html';";
//echo "},";
//echo "15000);"; // 15 seconds timeout
//echo "</script>";
echo "<p>Email, name, or comment fields were not filled out.</p> <p>Please click the <a href=\"javascript: history.go(-1)\">back button</a> on your browser, fill out all of the aforementioned fields, and re-submit the form.</p>";
//<p>This page will otherwise re-direct you in 15 seconds.
echo "<p>Thank you.</p>";
exit;

  }

else {
//Processing email form
 //Email information

  //send email
//header("refresh: 5; url=/about.html");
//echo "<script>";
//echo "setTimeout(function ()";
//echo "{";
//echo "window.location.href = '/about.html';";
//echo "},";
//echo "5000)"; // 5 seconds timeout
//echo "</script>";
 mail($admin_email, "$subject",$message, "From:" . $email);

  //Email response
  echo "<p>Comments submitted successfully!  Thank you for contacting us. Click  <a href=\"/about.html\">here</a> to return to the \"About\" page.</p>";
//<p>Returning you to the <a href=\"/about.html\">About Us</a> page in 5 seconds.</p>";
exit;

}
?>
</p>
</div>
</div>
</div>
</div>

<!-- STATIC doc END =========================================== -->
</div>
</div>
<!-- FONT RESIZE FUNCTION -->
<div style="display:none" id="font_size_attributes">.abt,.list_box li,p</div> <!-- DO NOT TOUCH -->

</div>
<!-- Footer BEGIN ========================================= -->
<!-- <footer id="footer">
<div id="footerContent">
<h5></h5>
</div>
</footer> -->
<div class="container">
<div class="row">
<!--Start disclaimer & privacy links. Created with div -->
<div class="footer-link-a">
<a href="/disclaimer.html">Disclaimer</a> | <a href="/privacy.html">Privacy</a>
</div>
<!-- End disclaimer & privacy links. Created with div -->
<!-- Start advertisement placeholder
<div class="col-xs-12">
<div class="row">
<div class="col-xs-12 adv_container">
<img src="/static/img/adv.png" class="img-responsive" alt="" />
</div>
</div>
</div>
End advertisement placeholder -->
</div>
</div>

<!-- Footer END =========================================== -->
<script type="text/javascript" language="javascript" src="/static/js/jquery.jfontsize-1.0.js"></script>
<script type="text/javascript" language="javascript">
font_size_hand();
read_font_size();
$('.jfontsize-button').bind('click', (function(e){write_font_size()}));
</script>
</body>
</html>

