'''
Created on Jun 7, 2014

@author: awacker
'''

from datetime import datetime, timedelta

user_sesion_manager = None

class UserSite:
    def __init__(self):
        self.last_search_expression = ''
        self.last_content = ''
        self.last_doc = ''
        self.records = 0
        self.current_page = 1
        self.step = 10
        self.COOKIE = ''
        self.dir_sort = ''
        self.col_sort = 4
        self.request_status = 0
        self.dir_sort_type = ''
        self.dir_sort_sby = ''
        self.btype = ''
        
    def getDiapason(self):        
        return '[%s - %s]' % (str(self.current_page * self.step - self.step + 1), str(self.current_page * self.step),)       
    
class UserSession:
    def __init__(self, user_id):
        self.user_id = user_id
        self.expired_time = datetime.utcnow()
        self.usitc = UserSite()
        self.cross = UserSite()
        self.csms = UserSite()
        self.ports = UserSite()
        
        
class UserSessionContaner:
    def __init__(self):
        self.items = []
        self.expired_minutes = 10
    def GetUserSession(self, user):
        result = None
        searche_useer = user.split('|')[3]
        old_items = self.items
        new_items = []
        for us_session in old_items:
            delta = datetime.utcnow() - us_session.expired_time           
            if delta < timedelta(minutes=self.expired_minutes):
                new_items.append(us_session)
            if us_session.user_id == searche_useer:
                us_session.expired_time = datetime.utcnow()
                result = us_session                
        self.items = new_items
        if result is None:
            new_user = UserSession(searche_useer)
            self.items.append(new_user)
            result = new_user
        return result
            

def get_user_sesion_manager():
    return user_sesion_manager


user_sesion_manager = UserSessionContaner()
