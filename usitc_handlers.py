import logging
import tornado.web
from tornado.httputil import url_concat
from tornado import httpclient

from session import get_user_sesion_manager

from usitc_search_processing import usitc_main_request, usitc_table_request, usitc_index_request
from handlers import BaseHandler

import urllib
import urlparse

import pymongo
#from pymongo import Connection
import json
from bson import json_util
import settings
import operator

USITC_context_url = r'/usitc'

caption = 'HTSUS'
list_view_html_template = 'usitc/usitc_index.html'
doc_view_html_template = 'usitc/usitc_doc.html'

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36'


######### usitcMainHandler =====================================================================================
class usitcMainHandler(BaseHandler):
    def initialize(self):
        self.connection = pymongo.Connection(settings.MONGODB_HOST, settings.MONGODB_PORT)
        self.db = self.connection[settings.MONGODB_CONNECTION]
        self.collection = self.db['usitc']

#        try:
#            self.update_date = payload(json.dumps(self.collection.find_one({'type': 'info'}), default = json_util.default)).update_date
#        except:
#            self.update_date = ''
    
#    @tornado.web.asynchronous
    def get(self, doc_id=''):

        
        http_client = tornado.httpclient.AsyncHTTPClient()
        current_user = self.current_user.usitc
        if doc_id != '':
            current_user.request_status = 2
        else:
            current_user.doc_id = ''
#         params = urlparse.urlparse(self.request.uri)        
#         if params.path in ['/usitc/', '/usitc']:        
#             current_user.request_status = 0
            
        if current_user.request_status == 0:
            logging.info("==USITC========= LIST INITIAL REQUEST, chapters_references")
            current_user = self.current_user.usitc
            
            chapter = self.get_argument('chapter', None)
            print chapter
            print "====="
            if chapter != None:
                l = []
                chapter = chapter.split(' ')[0]
                chapter_items_list = list(self.collection.find({'chapter': int(chapter)}))
                
                print chapter_items_list
                
                for el in chapter_items_list:
                    level = 0
                    if el['Level'] != "": 
                        level = int(el['Level'])
                    if level > 0: level = level * 3
                    desc = '<div style="padding-left:5px;">' * level + el['Description'] + '</div>'
                    s = '<tr style="font-size: 19px;"><td><a name="' + el['HTS_No'] + el['Stat_Suffix'] + '" href="/cross?qu=' + el['HTS_No'] + '">' + el['HTS_No'] + '</a></td><td><a href="/cross?qu=' + el['HTS_No'] + el['Stat_Suffix'] + '">' + el['Stat_Suffix'] + '</a></td><td>' + desc + '</td><td>' + el['Unit_of_Quantity'] + '</td><td>' + el['Col_1_Rate'] + '</td><td>' + el['Special_Rate'] + '</td><td>' + el['Col_2_Rate'] + '</td></tr>'

                    l.append(s)
                
                
                content = ''.join(l)
                self.render(doc_view_html_template, caption=caption, usitc_url=USITC_context_url, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), content=content)
            
            else:
                       
                selection_list = list(self.collection.find({'type': { "$in": ['selection', 'chapter']}}))
                l = []
                for el in selection_list:
                    if el['type'] == 'selection':
                        l.append([[el['name'], el['description']]])
                    else:
                        l[-1].append(['Chapter ' + el['name'], USITC_context_url + '?chapter=' + el['name'], el['description']])
                              
                current_user.last_content = {'results': l} 
                self.render(list_view_html_template, caption=caption, usitc_url=USITC_context_url, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), result=current_user.last_content) 
            

        elif current_user.request_status == 1:
            logging.info("==USITC========= LIST RESULT REQUEST")
            if current_user.last_search_expression == '':
                current_user.last_content = {'error': 'Please enter a search value'}
                self.render(list_view_html_template, caption=caption, usitc_url=USITC_context_url, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), result=current_user.last_content)
                return
            
            current_user = self.current_user.usitc
            l = []
            fields = {
                      "HTS_No": 1,
                      "Stat_Suffix": 1,
                      "Description": 1,
                      "chapter": 1
                      }
            selection_list = list(self.db.command("text", "usitc",
                                         search=current_user.last_search_expression,
                                         project=fields
                                         )['results'])
            
            for el in selection_list:  # sorted(ports_list):
                a = el['obj']
                a['link'] = '/usitc?chapter=' + str(el['obj']['chapter']) + '#' + el['obj']['HTS_No']
                a['name'] = el['obj']['HTS_No'] + el['obj']['Stat_Suffix']
                a['match'] = el['obj']['Description']
                l.append(a)
            l.sort(key=operator.itemgetter("HTS_No"))
         
            current_user.last_content = {"searchResults": l}
            current_user.request_status = 0
            print current_user.last_search_expression
            self.render(list_view_html_template, caption=caption, usitc_url=USITC_context_url, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), result=current_user.last_content)

        elif current_user.request_status == 2:
            logging.info("==USITC========= TABLE VIEW REQUEST")

            if doc_id == '':
                doc_id = current_user.last_doc
            headers = {
                   'User-Agent': user_agent,
                   } 
            url = 'http://hts.usitc.gov/' + urllib.quote(doc_id)

            req = tornado.httpclient.HTTPRequest(
                                              url=url,
                                              headers=headers
                                              )
            http_client.fetch(req, callback=self.on_fetch_table)
        elif current_user.request_status == 3:
            logging.info("==USITC========= BACK TO LIST RESULT")
            current_user = self.current_user.usitc
            current_user.request_status = 1
            self.render(list_view_html_template, caption=caption, usitc_url=USITC_context_url, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), result=current_user.last_content)    
        
        current_user.last_doc = doc_id
              
     
    def on_fetch_index(self, response):

        current_user = self.current_user.usitc
        current_user.last_content = usitc_index_request(response, current_user, USITC_context_url)
        self.render(list_view_html_template, caption=caption, usitc_url=USITC_context_url, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), result=current_user.last_content)        

    def on_fetch_results(self, response):

        current_user = self.current_user.usitc
        current_user.last_content = usitc_main_request(response, current_user, USITC_context_url)
        self.render(list_view_html_template, caption=caption, usitc_url=USITC_context_url, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), result=current_user.last_content)

    def on_fetch_table(self, response):
        current_user = self.current_user.usitc
        content = usitc_table_request(response, current_user)
        self.render(doc_view_html_template, caption=caption, usitc_url=USITC_context_url, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), content=content)

    def post(self):
        
        current_user = self.current_user.usitc

        if self.check_arguments("go"):
            current_user.current_page = 1
            current_user.request_status = 1
            current_user.records = 0
            current_user.last_search_expression = self.get_argument("qu")

        elif self.check_arguments("chapters_references"): 
            current_user.request_status = 0
            current_user.records = 0
            current_user.last_search_expression = ''
                            
        elif self.check_arguments("backtolist"):             
            current_user.request_status = 3
                                               
        self.redirect(USITC_context_url)              


