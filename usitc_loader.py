import urllib2
from bs4 import BeautifulSoup
import pymongo
import datetime
import settings
import time
from datetime import date
import traceback

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36'
USITS_context_url = r'/usits'   

def error_message(msg):
    global error_contaner
    error_contaner.append(msg)
    print(msg)

def get_db_collection(drop=False):
    try:
        c_path = 'mongodb://%s:%s' % (settings.MONGODB_HOST, settings.MONGODB_PORT,)
        connection = pymongo.Connection(c_path)
        database = connection[settings.MONGODB_CONNECTION]
        if drop:
            database['usitc'].drop()
        return database['usitc']
    except:
        error_message('Error: Unable to Connect ' + c_path)
        return None

def get_usits_items_list_by_chapter():
    http_path = 'http://hts.usitc.gov/by_chapter.html'

    print('$$$ Read all usits items on %s' % http_path)
    try:
        req = urllib2.Request(http_path, headers={'User-Agent': user_agent})
        return urllib2.urlopen(req, timeout=10).read().decode('utf-8', 'ignore')
    except IOError as err:
        error_message("$$$ HTTP READ ERROR !!! Original message: {0}".format(err))
        exit

def try_read(http_path):

    print('$$$ Read all usits items on %s' % http_path)
    try:
        req = urllib2.Request(http_path, headers={'User-Agent': user_agent})
        return urllib2.urlopen(req, timeout=10).read().decode('cp1252', 'ignore')
    except urllib2.URLError, e:
        if e.code == 404:
            return '404'       
    except IOError as err:
        error_message("$$$ HTTP READ ERROR !!! Original message: {0}".format(err))
        exit

def get_usits_items_list():
    year = str(date.today().year)#[-2:]
    release = 3
    while release > -1:
#        http_path = 'http://www.usitc.gov/tata/hts/%s0%s_hts_delimited.txt' % (year, release)
                
        http_path = 'http://www.usitc.gov/documents/%s_htsa_basic_delimited.txt' % (year) #, release
        r = try_read(http_path)
        if r != "404": return r
        else:
            error_message("!!! HTTP READ ERROR FOR {0}".format(http_path))
        release -= 1

    
def save_to_db(chapters_html, items_list_html):
    
    def get_value_by_name(titles, line, name):       
        for i in range(len(titles)):
            if titles[i] == name:
                return line.split('|')[i].strip()
        

    soup = BeautifulSoup(chapters_html).find('table') 
    
    if soup == None: 
        return {'error': 'No documents matched your query'}
    l = []
    collection = get_db_collection(True)
    if collection == None: return
    selection_id = ''
    for tr in soup.findAll('tr'):

        s = tr.getText().strip()

        if not s: continue
        if s.startswith('SECTION'):
            selection_id = collection.insert({'type': "selection",
                       'name': s.split(':')[0],
                       'description': ':'.join(s.split(':')[1:])
                       })
        elif s.startswith('Chapter'):

            collection.insert({'type': "chapter",
                       'name': s.split(' ')[1],
                       'description': s.split(' ', 2)[2],
                       'selection_id': selection_id
                       })

    if len(items_list_html.split('\r')) > 0:
        titles = items_list_html.split('\r')[0].split('|')
        columns_number = len(titles) - 1
        
        p = 0
        line = ''
        for s in items_list_html:

            line += s
            if line != '': #p == 0 and 
                if s == "\r":
                    print line
                    line = ''
                    p = 0
                    
            if s == "|":
                 p += 1
                 if p == columns_number:
                     p = 0

                     try:
                         HTS_No = get_value_by_name(titles, line, 'HTS No.')# .replace('con.)', '').replace('Con.)', '')  # split('(con')

                         print "==========", HTS_No
                         if HTS_No != 'HTS No.':
                             if HTS_No != '':
                                 chapter = int(HTS_No[:2])
                             
                             level = get_value_by_name(titles, line, 'Level')
                             if level != "" and HTS_No[-2:] != "on":
                                 collection.insert({
                                                'type': "item",
                                                'chapter': chapter,
                                                'HTS_No': HTS_No,
                                                'Stat_Suffix': get_value_by_name(titles, line, 'Stat Suffix'),
                                                'Level': level,
                                                'Description': get_value_by_name(titles, line, 'Description'),
                                                'Unit_of_Quantity': get_value_by_name(titles, line, 'Unit of Quantity'),
                                                'Col_1_Rate': get_value_by_name(titles, line, 'Col. 1 Rate'),
                                                'Special_Rate': get_value_by_name(titles, line, 'Special Rate').replace(',', ', '),
                                                'Col_2_Rate': get_value_by_name(titles, line, 'Col. 2 Rate')
                                                })
                     except Exception as err:
#                        print('==========================================================================================')
#                        print("Type: {0} / Original massage: {1} /n {2}".format(type(err), err, traceback.format_exc(),))
#                        print HTS_No
#                        print line
                    
                        pass

                     line = ''

    collection.ensure_index("Description", key={"Description":'text', "HTS_No":'text'}, name='Description_text', v=1)


error_contaner = []
chapters_html = get_usits_items_list_by_chapter()
if chapters_html:
    items_list_html = get_usits_items_list()

    save_to_db(chapters_html, items_list_html)
print("$$$ HTSUS Data Updating was finished")
# if len(error_contaner)>0:
#     print("")
#     print("$$$ Error report ------------------------------------------------------------")
#     for el in error_contaner:
#         print el 
