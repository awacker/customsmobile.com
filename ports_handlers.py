import logging
import tornado.web
from tornado.httputil import url_concat
from tornado import httpclient

from session import get_user_sesion_manager

from ports_search_processing import ports_main_request, ports_office_request
from handlers import BaseHandler

import urllib
import urlparse

import pymongo
from pymongo import * #Connection
import json
from bson import json_util

import settings

PORTS_context_url = r'/ports'

caption = 'PORTS'
list_view_html_template = 'ports/ports_index.html'
doc_view_html_template = 'ports/ports_doc.html'

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36'

class payload(object):
    def __init__(self, j):
        self.__dict__ = json.loads(j)

######### portsMainHandler =====================================================================================
class portsMainHandler(BaseHandler):
    def initialize(self):
        self.connection = pymongo.Connection(settings.MONGODB_HOST, settings.MONGODB_PORT)
        self.db = self.connection[settings.MONGODB_CONNECTION]
        self.collection = self.db['ports']
        try:
            self.update_date = payload(json.dumps(self.collection.find_one({'type': 'info'}), default=json_util.default)).update_date
        except:
            self.update_date = ''
            
#    @tornado.web.asynchronous
    def get(self, doc_id=''):
      
        http_client = tornado.httpclient.AsyncHTTPClient()
        current_user = self.current_user.ports
        
        if current_user.request_status == 3:
            logging.info("==PORTS========= BACK TO LIST RESULT")
            current_user.request_status = 0

            self.render("ports_index.html", ports_url=PORTS_context_url,
                     state=current_user.btype, result=current_user.last_content, update_date=self.update_date)
            
            return  
        
        params = urlparse.urlparse(self.request.uri)

        if params.path <> PORTS_context_url and params.path <> PORTS_context_url + '/' and params.path[len(PORTS_context_url):].split('/')[3] == 'field-office':
            logging.info("==PORTS========= Operation Office REQUEST")
            
            port = payload(json.dumps(self.collection.find_one({'office_href': params.path}), default=json_util.default))            
            
            current_user = self.current_user.ports

            content = port.office_body
            self.render(doc_view_html_template, caption=caption, ports_url=PORTS_context_url,
                     state=current_user.btype, content=content, update_date=self.update_date)   
            
        elif params.path <> PORTS_context_url and params.path <> PORTS_context_url + '/' and len(params.path[len(PORTS_context_url):].split('/')[3]) > 3:
            logging.info("==PORTS========= Port Name REQUEST")            
            
            port = payload(json.dumps(self.collection.find_one({'port_href': params.path}), default=json_util.default))            
            
            current_user = self.current_user.ports

            content = port.port_body
            self.render(doc_view_html_template, caption=caption, ports_url=PORTS_context_url,
                     state=current_user.btype, content=content, update_date=self.update_date) 

        else:
            logging.info("==PORTS========= LIST RESULT REQUEST")
            current_user = self.current_user.ports

            if current_user.btype == '':
                current_user.last_content = {'error': 'Please select a port from above'}
                self.render(list_view_html_template, caption=caption, ports_url=PORTS_context_url,
                                state=current_user.btype, result=current_user.last_content, update_date=self.update_date)
                return
     
            l = []
            fields = {
                      "port_href": 1,
                      "port_name": 1,
                      "location": 1,
                      "office_href": 1,
                      "office": 1
                      }
            if current_user.btype != "all":
                try:
                    ports_list = list(self.db.command("text", "ports",
                                         search='", ' + current_user.btype + '"',
                                         project=fields
                                         )['results'])
                except pymongo.errors.OperationFailure as err:
                    current_user.last_content = {'error': 'Search by item is not possible, the creation of the index was not completed correctly'}
                    self.render(list_view_html_template, caption=caption, ports_url=PORTS_context_url,
                                state=current_user.btype, result=current_user.last_content, update_date=self.update_date)
                    return
                for el in sorted(ports_list):
                    l.append(el['obj'])
                
            else:
                ports_list = list(self.collection.find({'type': 'item'},
                                                       fields).sort("port_name", 1))

                for el in ports_list:
                    l.append(el)

            results = {"results": l}
  
            current_user.last_content = {"results": l}

            self.render(list_view_html_template, caption=caption, ports_url=PORTS_context_url,
                      state=current_user.btype, result=current_user.last_content, update_date=self.update_date)
          
            
    def on_fetch_office(self, response):
        current_user = self.current_user.ports

        content = ports_office_request(response, current_user, PORTS_context_url)
        self.render(doc_view_html_template, caption=caption, ports_url=PORTS_context_url,
                     state=current_user.btype, content=content)        
 
    def on_fetch_results(self, response):

        current_user = self.current_user.ports

        current_user.last_content = ports_main_request(response, current_user, PORTS_context_url)
        
     #   print current_user.btype
      #  self.write(current_user.last_content)
        self.render(list_view_html_template, caption=caption, ports_url=PORTS_context_url,
                     state=current_user.btype, result=current_user.last_content)
        
    def post(self):

        current_user = self.current_user.ports        
        if self.check_arguments("state"):
            current_user.btype = self.get_argument("state")     
            
        if self.check_arguments("backtolist"):            
            current_user.request_status = 3         
        
        self.redirect(PORTS_context_url)              
