<?xml version="1.0" encoding="windows-1252"?>
<xsl:stylesheet version="2.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
       
<xsl:template match="/">

  <html>
    <header>
    <script type="text/javascript">

      window.onload = function()
      {
      var targetFrame = parent.parent.document.getElementById('TopOfPage');
      var targetDoc = null;
      var isFireFox=navigator.userAgent.match(/firefox/i);

      if (targetFrame.contentDocument)
      targetDoc = targetFrame.contentDocument;
      else if (targetFrame.contentWindow)
      targetDoc = targetFrame.contentWindow.document;
      else if (targetFrame.document)
      targetDoc = targetFrame.document;

      <xsl:variable name="ChaptID" select="chapter/@id"/>
      <xsl:variable name="SectionID" select="chapter/section"/>

      targetDoc.getElementById('PDF').href="http://www.usitc.gov/publications/docs/tata/hts/bychapter/1400C<xsl:value-of select="substring(format-number($ChaptID, '00'),1,2)"/>.pdf";
      targetDoc.getElementById('Rates').className='active';
      targetDoc.getElementById('HeadNotes').className='';
      targetDoc.getElementById('PDF').className='';
      targetDoc.getElementById('AlphIdx').className='';
      //targetDoc.getElementById('AcrobatLogo').className='';
      parent.document.getElementById('headnotes').style.display="none";
      parent.document.getElementById('headnotes').src="headers/Chapter Notes <xsl:value-of select="format-number($ChaptID, '00')"/>.html";
      parent.document.getElementById('sectionnotes').style.display="none";
      parent.document.getElementById('sectionnotes').src="headers/Section Notes <xsl:value-of select="$SectionID"/>.html";
      
//      parent.document.getElementById('tableheader').src="Table <xsl:value-of select="format-number($ChaptID, '00')"/>.xml#TopHead";
      //      parent.document.getElementById('tableheader').style.display="inline";

      parent.document.getElementById('showframe').style.display="inline";
      setTableWidth();

      //      window.location = window.location + "#TableStart";
      //      window.scrollBy(0, 13);
      
//      if (isFireFox) {
//       if (parent.document.getElementById('showframe').className!="hidden") {
//          window.location = window.location; //makes Firefox move to anchor
//          }
//        }

      }
      
      window.onresize = function()
      {
      setTableWidth()
      }

      function setTableWidth() {
      var x_dim = parent.document.getElementById('TopOfPage').clientWidth-200 + "px";
      var theTable = document.getElementById('HTSTable');
      var theTitle = document.getElementById('HTSTitle');

      theTable.style.width = x_dim;
      theTitle.style.width = x_dim;
      }

    </script>

    <style>
      table, th, td {
      font-family:arial;
      font-size:12px;
      padding:0px 1px;
      margin:0px;
      }

      h1 {
      font-size:16px;
      margin: 1px;
      padding: 0px;
      }

      h2 {
      font-size:10px;
      font-weight:bold;
      margin: 1px;
      padding: 0px;
      }

      div {
      font-family:arial;
      font-size:12px;
      padding: 0px;
      }

      #footnoteanchor {
      font-family:arial;
      font-size:12px;
      text-decoration:italics underline;
      }

    </style>
    </header>
    <body>
        <xsl:apply-templates select="chapter"/>
    </body>
  </html>
</xsl:template>


<xsl:template match="chapter">

<table cellspacing="0" cellpadding="0" border="0" id="HTSTitle" rules="cols" style="width:100%;margin:1px;padding:0px;">
<tr><td align="center">
  <h1>
    Harmonized Tariff Schedule of the United States (<xsl:value-of select="year"/>) <xsl:value-of select="supplement"/>
  </h1>
  <h2>Annotated for Statistical Reporting Purposes</h2>
</td></tr>
</table>


<a name="TopHead"/>
    <table border="1" id="HTSTable" frame="border" rules="cols" style="width:100%;">
      <a name="TableStart"/>
     <thead>
      <tr align="center">
          <th rowspan="3">Heading/ SubHeading</th>
          <th rowspan="3">Stat Suffix</th>
	    <th rowspan="3">Article Description</th>
	    <th rowspan="3">Unit of Quantity</th>
	    <th colspan="3">Rates of Duty</th>
      </tr>
      <tr align="center">
	    <th colspan="2">1</th>
	    <th rowspan="2">2</th>
        </tr>
      <tr align="center">
	    <th>General</th>
	    <th>Special</th>
        </tr>
       <tr><td colspan="7"><hr/></td></tr>

     </thead>
     <tfoot>
	<tr><td align="center" colspan="7"/></tr>
     </tfoot>
     <tbody valign="top">

	<xsl:apply-templates select="htstable"/>
     </tbody>
    </table>
  <xsl:for-each select="//footnote[count(. | key('NoteID', @ID)[1]) = 1]">
    <div style="display:inline;"><a>
      <xsl:attribute name="name">
        <xsl:value-of select="@ID"/>
      </xsl:attribute>
      <xsl:value-of select="@ID"/>/
    </a><xsl:value-of select="@note"/></div>
    <br/>
  </xsl:for-each>
  <xsl:apply-templates select="endnotes"/>
  
</xsl:template>

  
<xsl:template match="link">
  <a>
    <xsl:variable name="subChapt" select="."/>
    <xsl:attribute name="href">
      Table <xsl:value-of select="substring($subChapt,1,4)"/>.xml#<xsl:value-of select="."/></xsl:attribute>
    <xsl:value-of select="."/>
  </a>
</xsl:template>

  <xsl:key name="NoteID" match="//footnote" use="@ID"/>
  
  <xsl:template match="endnotes">

    <xsl:for-each select="//footnote[count(. | key('NoteID', @ID)[1]) = 1]">
      <a>
      <xsl:attribute name="name">
        <xsl:value-of select="@ID"/>
      </xsl:attribute>
      <xsl:value-of select="@ID"/>/
    </a>
    <xsl:apply-templates select="@note"/>
    <br/>
    </xsl:for-each>
    
</xsl:template>

<xsl:template match="@note">
  <div><xsl:apply-templates/></div>
</xsl:template>
  
<xsl:template match="htstable">
  <xsl:apply-templates/>
</xsl:template>


<xsl:template match="description">

<table style="border-style:none; padding:0px; margin:0px"><tr>
	<xsl:if test="@lvlind &gt; 0"><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></xsl:if>
	<xsl:if test="@lvlind &gt; 1"><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></xsl:if>
	<xsl:if test="@lvlind &gt; 2"><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></xsl:if>
	<xsl:if test="@lvlind &gt; 3"><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></xsl:if>
	<xsl:if test="@lvlind &gt; 4"><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></xsl:if>
	<xsl:if test="@lvlind &gt; 5"><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></xsl:if>
	<xsl:if test="@lvlind &gt; 6"><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></xsl:if>
	<xsl:if test="@lvlind &gt; 7"><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></xsl:if>
	<xsl:if test="@lvlind &gt; 8"><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></xsl:if>
	<xsl:if test="@lvlind &gt; 9"><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></xsl:if>
	<xsl:if test="@lvlind &gt; 10"><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></xsl:if>
	<xsl:if test="@lvlind &gt; 11"><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></xsl:if>

<td><xsl:apply-templates/>
</td></tr></table>
</xsl:template>

<xsl:template match="footnote">     
 <a class="footnoteanchor">
 <xsl:attribute name="title">
   <xsl:value-of select="@note"/>
 </xsl:attribute>
 <xsl:attribute name="href">#<xsl:value-of select="@ID"/></xsl:attribute>
 <xsl:value-of select="@ID"/>/
</a>
</xsl:template>

<xsl:template match="htsrow | four_or_six_digit_row | superior_text_row | eight_digit_with_00_row | eight_digit_row | ten_digit_row">
  <xsl:if test="not(@print = 'CarryOver')">
    <tr>
      <xsl:attribute name="style">
        <xsl:choose>
          <xsl:when test="@lineno mod 2 = 0">background-color:#FFFFFF</xsl:when>
          <xsl:when test="@lineno mod 2 = 1">background-color:#E8E8E8</xsl:when>
        </xsl:choose>
      </xsl:attribute>

      <td><a>
        <xsl:attribute name="name"><xsl:value-of select="htsno"/>
          <xsl:if test="htsno/@print='10Digit' or hts10no='00'"><xsl:value-of select="hts10no"/></xsl:if>
        </xsl:attribute>
        <xsl:if test="not(htsno/@print='10Digit')"><xsl:value-of select="htsno"/></xsl:if>
      </a></td>
      <td>
        <a>
          <xsl:attribute name="title">Customs Rulings Online Search System</xsl:attribute>
          <xsl:attribute name="href">
            /cross?qu=<xsl:value-of select="htsno"/><xsl:value-of select="hts10no"/>
          </xsl:attribute>
          <xsl:apply-templates select="hts10no"/>
        </a>
      </td>
      <td><xsl:apply-templates select="description"/></td>
      <td><xsl:apply-templates select="units"/></td>
	<td><xsl:apply-templates select="mfn_tariff"/></td>
	<td><xsl:apply-templates select="special"/></td>
	<td><xsl:apply-templates select="other_tariff"/></td>
    </tr>
  </xsl:if>
</xsl:template>

<xsl:template match="hts10no">
      <xsl:apply-templates/>
</xsl:template>

<xsl:template match="units">
      <xsl:apply-templates select="unit"/>
</xsl:template>

<xsl:template match="unit">
      <xsl:apply-templates/><br/>
</xsl:template>

<xsl:template match="mfn_tariff">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="special">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="other_tariff">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="tariff">
	<xsl:apply-templates/><br/>
</xsl:template>

  <xsl:template match="trdagr">
    <span style="color:blue">
      <xsl:attribute name="title">
        <xsl:choose>
          <xsl:when test=". = 'A'">Generalized System of Preferences (GSP) (duty-free treatment)</xsl:when>
          <xsl:when test=". = 'A*'">Certain countries excluded from GSP eligibility for that HTS subheading (duty-free treatment)</xsl:when>
          <xsl:when test=". = 'A+'">Only imports from least-developed beneficiary developing countries eligible for GSP under that subheading( duty-free treatment)</xsl:when>
          <xsl:when test=". = 'AU'">Australia Special Rate</xsl:when>
          <xsl:when test=". = 'B'"> Automotive Products Trade Act (APTA) (duty-free treatment)</xsl:when>
          <xsl:when test=". = 'BH'">Bahrain Special Rate</xsl:when>
          <xsl:when test=". = 'C'">Agreement on Trade in Civil Aircraft  (duty-free treatment)</xsl:when>
          <xsl:when test=". = 'CA'">NAFTA for Canada (duty-free treatment)</xsl:when>
          <xsl:when test=". = 'CL'">Chile Special Rate</xsl:when>
		  <xsl:when test=". = 'CO'">Colombia Special Rate</xsl:when>
          <xsl:when test=". = 'D'">Africa Growth and Opportunity Act (AGOA) (duty-free treatment)</xsl:when>
          <xsl:when test=". = 'E'">Caribbean Basin Initiative (CBI)</xsl:when>
          <xsl:when test=". = 'E*'">Certain countries or products excluded from CBI eligibility</xsl:when>
          <xsl:when test=". = 'IL'">Israel Special Rate (duty-free treatment)</xsl:when>
          <xsl:when test=". = 'J'">Andean Trade Preference Act (ATPA)</xsl:when>
          <xsl:when test=". = 'J*'">Certain products excluded from ATPA eligibility</xsl:when>
          <xsl:when test=". = 'J+'">Andean Trade Promotion and Drug Eradication Act (ATPDEA)</xsl:when>
          <xsl:when test=". = 'JO'">Jordan Special Rate</xsl:when>
          <xsl:when test=". = 'K'">Agreement on Trade in Pharmaceutical Products (duty-free treatment)</xsl:when>
		  <xsl:when test=". = 'KR'">Korea Special Rate</xsl:when>
          <xsl:when test=". = 'L'">Uruguay Round Concessions on Intermediate Chemicals for Dyes (duty-free treatment)</xsl:when>
          <xsl:when test=". = 'MA'">Morocco Special Rate</xsl:when>
          <xsl:when test=". = 'MX'">NAFTA for Mexico</xsl:when>
          <xsl:when test=". = 'OM'">Oman Special Rate</xsl:when>
          <xsl:when test=". = 'P'">Dominican Republic-Central American Special Rate (DR-CAFTA)</xsl:when>
          <xsl:when test=". = 'P+'">Dominican Republic-Central American Plus Rate (DR-CAFTA Plus)</xsl:when>
		  <xsl:when test=". = 'PA'">Panama Special Rate</xsl:when>
          <xsl:when test=". = 'PE'">Peru Special Rate</xsl:when>
          <xsl:when test=". = 'R'">Caribbean Basin Trade Partnership Act (CBTPA)</xsl:when>
          <xsl:when test=". = 'SG'">Singapore Special Rate</xsl:when>
        </xsl:choose>
      </xsl:attribute>
      <xsl:value-of select="."/>
    </span>
  </xsl:template>

<xsl:template match="comma">,<wbr/></xsl:template>
<xsl:template match="dash">-<wbr/></xsl:template>

<xsl:template match="rate">
	<xsl:choose>
   <xsl:when test="value = '0'">Free</xsl:when>
   <xsl:when test="unit = '%'">
	<xsl:value-of select="round(value * 1000) div 10"/>%
   </xsl:when>
   <xsl:when test="value &lt; 1">	
	<xsl:value-of select="round(value * 1000) div 10"/><xsl:text>�/</xsl:text><xsl:value-of select="unit"/>
   </xsl:when>
   
	<xsl:otherwise>
	<xsl:text>$</xsl:text><xsl:value-of select="value"/><xsl:text>/</xsl:text><xsl:value-of select="unit"/>
	</xsl:otherwise>
</xsl:choose>
	<xsl:if test="position()!=last()">
        <xsl:text> + </xsl:text>
      </xsl:if>
</xsl:template>

<xsl:template match="sup">
<sup><xsl:apply-templates/></sup>
</xsl:template>

<xsl:template match="sub">
<sub><xsl:apply-templates/></sub>
</xsl:template>

<xsl:template match="i">
<i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="c">
  <br/><center><xsl:apply-templates/></center><br/>
</xsl:template>

<xsl:template match="p">
  <p style="text-indent:-.06in;margin-left:.06in;margin-top:0;margin-bottom:0"><xsl:apply-templates/></p>
</xsl:template>


</xsl:stylesheet>