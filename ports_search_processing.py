
from bs4 import BeautifulSoup


def getContent(current_user_structure, page_Html, path):

    if page_Html == None:
        return {'error': 'Port listings are not currently available'}

    soup = BeautifulSoup(page_Html)
    table = soup.find('table', {'summary': 'All Ports of Entry'}) 
    if table == None: 
        return {'error': 'Please select a port from the drop-down list above'}
    l = []
    for tr in table.findAll("tr", {"class": ["odd", "even", "odd views-row-first", "odd views-row-last", ]}):
        tdLi = tr.findAll('td')
        l.append({
            'port_href': path + tdLi[0].find('a')['href'],
            'port_name': tdLi[0].find('a').getText(),
            'location': tdLi[1].getText().strip(),
            'office_href':  path + tdLi[2].findAll('a')[-1]['href'] if len(tdLi[2].findAll('a')) > 1 else '#',
            'office': tdLi[2].getText(),
        })
    return {'results': l}

def ports_main_request(response, current_user_structure, path):
    page_Html = response.body
    
    return getContent(current_user_structure, page_Html, path)

def ports_office_request(response, current_user_structure, path):

    page_Html = response.body
    if page_Html == None:
        return 'Document is empty'    
    
    soup = BeautifulSoup(page_Html).find('div', {'class': 'region-inner region-content-inner'})

    broker = soup.find('div', {'class': 'field field-name-field-brokers field-type-link-field field-label-inline clearfix'})
    if broker:
        broker.replace_with('')

    return soup



