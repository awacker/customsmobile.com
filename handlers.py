import logging
import tornado.web
from tornado.httputil import url_concat
from tornado import httpclient

from session import get_user_sesion_manager


import urllib
import urlparse

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36'


class BaseHandler(tornado.web.RequestHandler):
    def check_arguments(self, name):
        result = False
        l = len(name)
        for el in self.request.arguments:
            if el[:l] == name:
                result = True    
        return result

    def get_current_user(self):        
        user_obj = get_user_sesion_manager().GetUserSession(self.xsrf_token)  
        if not user_obj: return None
        return user_obj

class IndexHandler(BaseHandler):
    
    def get(self):
        self.render("index.html")
         

