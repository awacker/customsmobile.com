import logging
import tornado.web
from tornado.httputil import url_concat
from tornado import httpclient, gen
from tornado.escape import json_encode

from session import get_user_sesion_manager

from cross_search_processing import cross_main_request, cross_doc_request, cross_doc_header_request
from handlers import BaseHandler

import urllib
import urlparse
import math

import pymongo
#from pymongo import Connection
import json
from bson import json_util
import settings
import operator
from datetime import datetime
import time
from elasticsearch import Elasticsearch

CROSS_context_url = r'/cross'

caption = 'RULINGS'
list_view_html_template = 'cross/cross_index.html'
doc_view_html_template = 'cross/cross_doc.html'

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36'

class payload(object):
    def __init__(self, j):
        self.__dict__ = json.loads(j)

class DownloadHandler(BaseHandler):
    @tornado.web.asynchronous
    def get(self, href=''):
        if href != '':
            self.set_header('Content-Type', 'application/octet-stream')
            http_client = httpclient.AsyncHTTPClient()
            http_client.fetch('http://rulings.cbp.gov/' + href, callback=self.on_fetch_download)
    
    def on_fetch_download(self, response):
            self.write(response.body)
            self.finish()


######### crossHandler_doc =====================================================================================
class crossHandler_doc(BaseHandler):
    def initialize(self):
        self.es = Elasticsearch()

#    @tornado.web.asynchronous
#    @tornado.gen.engine
    def get(self, *args, **kwargs):
        logging.info("==CROSS========= GET DOC VIEW")
        doc_id = self.get_argument('doc_id', None)
        if not doc_id:
            raise tornado.web.HTTPError(412)
        real_doc_id = doc_id.split()
        real_doc_id = real_doc_id[len(real_doc_id) - 1].lower()
        current_user = self.current_user.cross
        res_count = 0 
        try:
            res = self.es.search(index="node1.cross",
                                body={"query": {
                                         "regexp" : { 
                                                    "real_doc_id": ".*" + real_doc_id,
                                                    }
                                                 },
                                      },
                                fields=["doc_id",
                                        'id',
                                        'date',
                                        'category',
                                        'real_doc_id',
                                        'tariff_no',
                                        'doc_html_content'
                                        ]    
                            )
            
            res_count = len(res['hits']['hits'])
        except:
            pass
        if res_count > 0:
    	    if res['hits']['hits'][0]['fields']['real_doc_id'][0] != real_doc_id:
    		res_count = 0
        if res_count < 1:
            d = {'error': "Document %s is not found" % real_doc_id}
 
            self.render(doc_view_html_template, caption=caption, cross_url=CROSS_context_url, diapason='', search_expression=current_user.last_search_expression,
                      records=str(current_user.records), current_page=str(current_user.current_page),
                      step=current_user.step, col_sort=current_user.col_sort, result=d)
            return            

        content = cross_doc_request(res['hits']['hits'][0]['fields']['doc_html_content'][0])
        
        dt_obj = datetime.strptime(res['hits']['hits'][0]['fields']['date'][0], "%Y-%m-%dT%H:%M:%S")
        date_str = dt_obj.strftime("%b %d, %Y")
        
        d = {'id': res['hits']['hits'][0]['fields']['doc_id'][0],
            'date': date_str,
            'category': res['hits']['hits'][0]['fields']['category'][0],
            'tariff_no': res['hits']['hits'][0]['fields']['tariff_no'][0],
            'content': content}

        self.render(doc_view_html_template, caption=caption, cross_url=CROSS_context_url, diapason='', search_expression=current_user.last_search_expression,
                      records=str(current_user.records), current_page=str(current_user.current_page),
                      step=current_user.step, col_sort=current_user.col_sort, result=d)
        


######### crossMainHandler =====================================================================================
class crossMainHandler(BaseHandler):
#    def initialize(self):
#        self.connection = pymongo.Connection(settings.MONGODB_HOST, settings.MONGODB_PORT)
#        self.db = self.connection[settings.MONGODB_CONNECTION]
#        self.collection = self.db['cross']
    
#    @tornado.web.asynchronous
    def get(self, doc_id=''):
        
        http_client = tornado.httpclient.AsyncHTTPClient()
        current_user = self.current_user.cross
        
        params = urlparse.urlparse(self.request.uri)
        if 'qu' in urlparse.parse_qs(params.query): 
            logging.info("==CROSS========= LIST RESULT REQUEST 1")        
            current_user.last_search_expression = ''.join(urlparse.parse_qs(params.query)['qu'])
            current_user.request_status = 0
            current_user.records = 0
            current_user.current_page = 1  

        if doc_id != '':
            current_user.request_status = 2
        else:
            current_user.doc_id = ''
        
        if current_user.request_status == 0:
            logging.info("==CROSS========= LIST RESULT REQUEST")
 
            if current_user.last_search_expression == '':
                current_user.last_content = {'error': 'Your search, your business: visit our Facebook page to read about what we\'re doing to actively protect your privacy. '}
                
                self.render(list_view_html_template, caption=caption, cross_url=CROSS_context_url, diapason='', search_expression=current_user.last_search_expression,
                     records=str(current_user.records), current_page=str(current_user.current_page),
                     step=current_user.step, col_sort=current_user.col_sort, result=current_user.last_content)

                return
             
            current_user = self.current_user.cross
            l = []

            start_record = (current_user.current_page - 1) * current_user.step
            es = Elasticsearch()
            
            if current_user.col_sort == '4':
                s_condition = "rank:desc"
 
            elif current_user.col_sort == '12':
                s_condition = "date:asc"
 
            elif current_user.col_sort == '1':
                s_condition = "date:desc"    
                      
            try:
                res = es.search(index="node1.cross",
                                body={"query": {
                                         "simple_query_string" : {        
                                                                  "query": current_user.last_search_expression,
                                                                  "fields": ["doc_html_content", "doc_id"],
                                                                  "default_operator": "and"
                                                                  }
                                                 }
                                      },
                                fields=["doc_id",
                                        "date",
                                        "rank",
                                        "category",
                                        "file_name",
                                        "ruling_reference",
                                        "tariff_no",
                                        "related_modifies_liks",
                                        "related_references_liks",
                                        "related_revokes_liks",
                                        "related_revoking_liks",
                                        "related_modifying_liks",
                                        "real_doc_id"
                                        ],
                               # sort=s_condition,
                                size=current_user.step,
                                from_=start_record                                
                            )
            except:
                logging.info("==CROSS========= except LIST RESULT REQUEST") 
                current_user.last_content = {'error': 'Search Engine is not working'}

                self.render(list_view_html_template, caption=caption, cross_url=CROSS_context_url, diapason='', search_expression=current_user.last_search_expression,
                     records=str(current_user.records), current_page=str(current_user.current_page),
                     step=current_user.step, col_sort=current_user.col_sort, result=current_user.last_content)

                return     
                          

            current_user.records = res['hits']['total']
            if current_user.records == 0:
                current_user.last_content = {'error': 'No documents matched your query'}
                diapason = current_user.getDiapason()
                
                self.render(list_view_html_template, caption=caption, cross_url=CROSS_context_url, diapason=diapason, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), current_page=str(current_user.current_page),
                     step=current_user.step, col_sort=current_user.col_sort, result=current_user.last_content)

                return    
            
                 

            for el in res['hits']['hits']:
#                print el['fields']['doc_id'][0], el['fields']['real_doc_id'][0]
                
                dt_obj = datetime.strptime(el['fields']['date'][0], "%Y-%m-%dT%H:%M:%S")
                date_str = dt_obj.strftime("%b %d, %Y")
            
                el['fields']['date'] = date_str
                el['fields']['rank'] = el['fields']['rank'][0]
                el['fields']['category'] = el['fields']['category'][0]
                el['fields']['tariff_no'] = el['fields']['tariff_no'][0] if 'tariff_no' in el['fields'] else ''
                
                el['fields']['ruling_reference'] = el['fields']['ruling_reference'][0]             
                el['fields']['link'] = CROSS_context_url + '/docview?doc_id=' + el['fields']['doc_id'][0]
                el['fields']['id'] = el['fields']['doc_id'][0]
                el['fields']['related_modifies_liks'] = el['fields']['related_modifies_liks'][0] if 'related_modifies_liks' in el['fields'] else '' 
                el['fields']['related_references_liks'] = el['fields']['related_references_liks'][0] if 'related_references_liks' in el['fields'] else ''
                el['fields']['related_revokes_liks'] = el['fields']['related_revokes_liks'][0] if 'related_revokes_liks' in el['fields'] else ''
                el['fields']['related_revoking_liks'] = el['fields']['related_revoking_liks'][0] if 'related_revoking_liks' in el['fields'] else ''
                el['fields']['related_modifying_liks'] = el['fields']['related_modifying_liks'][0] if 'related_modifying_liks' in el['fields']  else ''
                
                l.append(el['fields'])            

            diap = int(current_user.getDiapason().split('- ')[1][:-1])

            
            current_user.last_content = {"results": l}
            current_user.request_status = 0

            diapason = current_user.getDiapason()
            
            self.render(list_view_html_template, caption=caption, cross_url=CROSS_context_url, diapason=diapason, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), current_page=str(current_user.current_page),
                     step=current_user.step, col_sort=current_user.col_sort, result=current_user.last_content)

        elif current_user.request_status == 3:
            logging.info("==CROSS========= BACK TO LIST RESULT")
            current_user = self.current_user.cross
            diapason = current_user.getDiapason()
            
            current_user.request_status = 1
            self.render(list_view_html_template, caption=caption, cross_url=CROSS_context_url, diapason=diapason, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), current_page=str(current_user.current_page),
                     step=current_user.step, col_sort=current_user.col_sort, result=current_user.last_content)     
        
        current_user.last_doc = doc_id     
      
    
    def post(self):

        current_user = self.current_user.cross

        if self.check_arguments("go"):
            current_user.current_page = 1
            current_user.request_status = 0
            current_user.records = 0
            current_user.step = int(self.get_argument("ps"))
            current_user.last_search_expression = self.get_argument("qu")

        elif self.check_arguments("next"): 
            current_user.current_page += 1
            current_user.step = int(self.get_argument("ps"))
            current_user.last_search_expression = self.get_argument("qu")

        elif self.check_arguments("end"): 
            current_user.current_page = int(math.ceil(float(current_user.records) / float(current_user.step)))
            current_user.step = int(self.get_argument("ps"))
            current_user.last_search_expression = self.get_argument("qu")
            
        elif self.check_arguments("prev") and current_user.current_page > 1:             
            current_user.current_page -= 1
            current_user.step = int(self.get_argument("ps"))
            current_user.last_search_expression = self.get_argument("qu")

        elif self.check_arguments("sda"):             
            current_user.dir_sort = 'asc'
            current_user.col_sort = 1

        elif self.check_arguments("sdd"):             
            current_user.dir_sort = 'desc'
            current_user.col_sort = 1
              
        elif self.check_arguments("snd"):
            current_user.dir_sort = 'desc'
            current_user.col_sort = 4
        
        elif self.check_arguments("backtolist"):             
            reuest_type = 0
            current_user.request_status = 3
                        
        if self.check_arguments("colSort"):
            current_user.dir_sort = 'desc'
            current_user.col_sort = self.get_argument("colSort")
                        
        self.redirect(CROSS_context_url)              
