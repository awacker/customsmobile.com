from bs4 import BeautifulSoup, Tag
import urlparse
import traceback
	
def getContent(current_user_structure, page_Html, path):
    if current_user_structure.last_search_expression == '':
#        return {'error': 'Please enter a search term above'}
        return {'error': 'Your search, your business: visit our Facebook page to read about what we\'re doing to actively protect your privacy.'}
    if page_Html == None:
        return {'error': 'Source site is not currently available'}

    soup = BeautifulSoup(page_Html)
    table = soup.find('table')
    if table == None: 
    	current_user_structure.records = 0
        return {'error': 'No documents matched your query'}
    paging_node = table.find('table')
    if paging_node == None: 
        return {'error': 'No documents matched your query'}
    paging_sub_node = paging_node.find('td', {'class': 'pageCountHeader'})
    if paging_sub_node == None:
        return {'error': 'No documents matched your query'}
    paging_value = paging_sub_node.next.split()
    current_user_structure.records = int(paging_value[3])
    l = []
    for tr in soup.findAll('table', {'class': 'results'})[1].contents[1:-1]:
        tdLi = tr.findAll("td", {"scope": ["row", ]})
        
        modifies_liks = []
        references_liks = []
        revokes_liks = []
        revoking_liks = []
        modifying_liks = []
        
        related_flag = 0
        for el in tdLi[4]:
        	if isinstance(el, Tag):
        		if el.has_attr('class') and el['class'][0] == 'modified_heading': related_flag = 1
        		elif el.has_attr('class') and el['class'][0] == 'referenced_heading': related_flag = 2
        		elif el.has_attr('class') and el['class'][0] == 'revoked_heading': related_flag = 3
        		elif el.has_attr('class') and el['class'][0] == 'revoking_heading': related_flag = 4
        		elif el.has_attr('class') and el['class'][0] == 'modifying_heading': related_flag = 5
        		elif el.has_attr('target'):
        			if related_flag == 1: modifies_liks.append('<a href="%s">%s</a>' % (path + '/docview?doc_id=' + el['href'].split('&')[0][13:], el.getText()))
        			elif related_flag == 2: references_liks.append('<a href="%s">%s</a>' % (path + '/docview?doc_id=' + el['href'].split('&')[0][13:], el.getText()))
        			elif related_flag == 3: revokes_liks.append('<a href="%s">%s</a>' % (path + '/docview?doc_id=' + el['href'].split('&')[0][13:], el.getText()))
        			elif related_flag == 4: revoking_liks.append('<a href="%s">%s</a>' % (path + '/docview?doc_id=' + el['href'].split('&')[0][13:], el.getText()))
        			elif related_flag == 5: modifying_liks.append('<a href="%s">%s</a>' % (path + '/docview?doc_id=' + el['href'].split('&')[0][13:], el.getText()))
        
        l.append({
            'date': tdLi[0].getText(),
            'rank': tdLi[1].getText(),
            'category': tdLi[2].find('br').next,
            'id': tdLi[2].findAll("a")[1].getText().strip(),
            'tariff_no': '; '.join(['<a href="%s">%s</a>' % (a['href'], a.getText().strip()) for a in tdLi[2].findAll("a")[2:]]),
            'ruling_reference': tdLi[3].getText(),
            'related_modifies_liks': '; '.join(modifies_liks),
    		'related_references_liks': '; '.join(references_liks),
    		'related_revokes_liks': '; '.join(revokes_liks),
    		'related_revoking_liks': '; '.join(revoking_liks),
    		'related_modifying_liks': '; '.join(modifying_liks),
    		
            'link': path + '/docview?doc_id=' + tdLi[2].findAll("a")[1].getText().strip()  # tdLi[2].findAll("a")[1]['href'].split('&')[0][13:],
        })
    return {'results': l}


def cross_main_request(response, current_user_structure, path):
    page_Html = response.body
    return getContent(current_user_structure, page_Html, path)

def cross_doc_request(response):
	soup = BeautifulSoup(response)
	soup.find('a').replaceWith("")
	ruling_code = ''
	try:
		ruling_code = soup.find('br').previous.extract()
		ruling_code = ruling_code.split(', ')[1]
		if ruling_code[0] == " ":
			ruling_code = ruling_code[1:]
		ruling_code = ruling_code[4:]
		ruling_code = ruling_code.split('CATEGORY')[0]
	except:
		pass
# print ruling_code
	soup.find('br').previous.replaceWith(ruling_code)
	return soup
	
	
def cross_doc_request1(response, current_user_structure, path):
	if response.body == None:
		return {'error': 'Document is empty'}
	try:
		soup = BeautifulSoup(response.body).find("tr", {"class": ["detail_body", "printer"], })
		if soup == None:
			return {'error': 'Document is empty'}
		brLi = soup.findAll('br')
		d = {'id': brLi[0].previous.extract(), 'date': brLi[0].next.extract(), 'name': brLi[1].next.extract(), 'category': brLi[2].next.extract(), 'tariff_no': brLi[3].next.extract()}
		d['tariff_no'] = d['tariff_no'].replace('TARIFF NO.: ', '').strip() if type(d['tariff_no']) == str and d['tariff_no'].count('TARIFF NO.: ') > 0 else ''
		d['category'] = d['category'].replace('CATEGORY:', '').strip()
		for i in range(4): brLi[i].extract()
		
		
		
		
		
		for doc in soup.findAll("a", {"target": ["detail", "_top", ]}):
			href = doc.get('href')
			if href[:11] == 'detail.asp?':
				doc_id = href.split('&')[0][14:]
				doc['href'] = path + '/docview?doc_id=' + doc.getText()
				doc['target'] = ''
				
				
		d['content'] = str(soup)
		return d
	except Exception as err:
		return ({'error': "ERROR! HTML processing exception <br>Original massage: {1} <br> {2}".format(type(err), err, traceback.format_exc(),)})
   
   
def cross_doc_header_request(response, path, doc_id):
	soup = BeautifulSoup(response.body)
	error = soup.find('td', {'class': 'error_message'})
	if error:
		return error.getText()

	for tr in soup.findAll('table', {'class': 'results'})[1].contents[1:-1]:
		tdLi = tr.findAll("td", {"scope": ["row", ]})
		tr_doc_id = '"' + tdLi[2].findAll("a")[1].getText().strip() + '"'

		if tr_doc_id.find(doc_id.replace('"', '')) > -1:
			return ({
					'date': tdLi[0].getText(),
					'category': tdLi[2].find('br').next,
					'id': tdLi[2].findAll("a")[1].getText().strip(),
					'tariff_no': '; '.join(['<a href="%s">%s</a>' % (a['href'], a.getText().strip()) for a in tdLi[2].findAll("a")[2:]]),
					'ruling_reference': tdLi[3].getText(),
					'related': '; '.join(['<a href="%s">%s</a>' % (path + '/docview?doc_id=' + r['href'].split('&')[0][13:], r.getText()) for r in tdLi[4].findAll("a")]),
					'link': path + '/docview?doc_id=' + tdLi[2].findAll("a")[1].getText().strip(),  # tdLi[2].findAll("a")[1]['href'].split('&')[0][13:],
					})

	return ''
	
