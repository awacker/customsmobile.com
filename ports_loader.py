import urllib2
from bs4 import BeautifulSoup
import pymongo
import datetime
import settings
import time

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36'
PORTS_context_url = r'/ports'   

def error_message(msg):
    global error_contaner
    error_contaner.append(msg)
    print(msg)

def get_db_collection(drop=False):
    try:
        c_path = 'mongodb://%s:%s' % (settings.MONGODB_HOST, settings.MONGODB_PORT,)
        connection = pymongo.Connection(c_path)
        database = connection[settings.MONGODB_CONNECTION]
        if drop:
            database['ports'].drop()
        return database['ports']
    except:
        error_message('Error: Unable to Connect ' + c_path)
        return None
        
def get_ports_list():
    http_path = 'http://www.cbp.gov/contact/ports/all'

    print('$$$ Read all ports on %s' % http_path)
    try:
        req = urllib2.Request(http_path, headers={'User-Agent': user_agent})
        return urllib2.urlopen(req, timeout=10).read().decode('utf-8')
    except IOError as err:
        error_message("$$$ HTTP READ ERROR !!! Original message: {0}".format(err))
        exit
        
def get_port_info(path):
    http_path = 'http://www.cbp.gov' + path

    print('--------------- Read info from %s' % http_path)
    try:
        req = urllib2.Request(http_path, headers={'User-Agent': user_agent})
        page_Html = urllib2.urlopen(req, timeout=10).read().decode('utf-8')
    except IOError as err:
        error_message("-ERROR!!!------ Original message: {0} for {1}".format(err, http_path))
        return 'Document is empty'   

    if page_Html == None:
        return 'Document is empty'    
    
    
    soup = BeautifulSoup(page_Html).find('div', {'class': 'region-inner region-content-inner'})

    broker = soup.find('div', {'class': 'field field-name-field-brokers field-type-link-field field-label-inline clearfix'})
    
    if broker:
        broker.replace_with('')

    return soup
    
    
def save_item_to_db(page_Html):
         
    soup = BeautifulSoup(page_Html)
    table = soup.find('table', {'summary': 'All Ports of Entry'}) 
    if table == None: 
        return {'error': 'Please select a port from the drop-down list above'}
    l = []
    collection = get_db_collection(True)
    if collection == None: return
    
#    collection.ensure_index("location", key={"location":'text'}, name='location_text', v=1)
    
#    return
#    x=0
    for tr in table.findAll("tr", {"class": ["odd", "even", "odd views-row-first", "odd views-row-last", ]}):
        tdLi = tr.findAll('td')
#        x+=1
#        if x>10: break

        p = get_port_info(tdLi[0].find('a')['href'])
        time.sleep(1)
        if len(tdLi[2].findAll('a')):
            o = get_port_info(tdLi[2].findAll('a')[-1]['href'])
            time.sleep(1)
        else:
            o = ''
       
        collection.insert({'type': 'item',
                           'port_name': str(tdLi[0].find('a').getText()).encode('utf-8'),
                           'port_href': PORTS_context_url + tdLi[0].find('a')['href'],
                           'location': str(tdLi[1].getText().strip()).encode('utf-8'),
                           'office_href':  str(PORTS_context_url + tdLi[2].findAll('a')[-1]['href'] if len(tdLi[2].findAll('a')) > 1 else '#').encode('utf-8'),
                           'office': str(tdLi[2].getText()).encode('utf-8'),
                           'port_body': p.encode('utf-8'),
                           'office_body': o.encode('utf-8')
                           })
        
        
        
    print("$$$ Index Creation")    
    collection.ensure_index("location", key={"location":'text'}, name='location_text', v=1)
    collection.insert({'type': 'info',
                       'update_date': datetime.datetime.now().strftime('%b %d %Y')
# The above line is a modification of Andrii's code, which was as follows:
#                      'update_date': str(datetime.datetime.now())[:19]                       
})

error_contaner = []
html = get_ports_list()
if html:
    save_item_to_db(html)
print("$$$ Ports Data Updating was finished")
if len(error_contaner) > 0:
    print("")
    print("$$$ Error report ------------------------------------------------------------")
    for el in error_contaner:
        print el 
