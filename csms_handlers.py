import logging
import tornado.web
from tornado.httputil import url_concat
from tornado import httpclient

from session import get_user_sesion_manager

from csms_search_processing import csms_main_request, csms_doc_request 
from handlers import BaseHandler

import urllib
import urlparse
import math

CSMS_context_url = r'/notices'

caption = 'NOTICES'
list_view_html_template = 'csms/csms_index.html'
doc_view_html_template = 'csms/csms_doc.html'

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36'

######### csmsMainHandler =====================================================================================
class csmsMainHandler(BaseHandler):
    
    @tornado.web.asynchronous
    def get(self, doc_id=''):
        
        http_client = tornado.httpclient.AsyncHTTPClient()
        current_user = self.current_user.csms
        
        opt = 0
        params = urlparse.urlparse(self.request.uri)

        if 'srch_argv' in urlparse.parse_qs(params.query): 
      
            current_user.last_search_expression = ''.join(urlparse.parse_qs(params.query)['srch_argv'])
            current_user.request_status = 0
            current_user.records = 0
            current_user.current_page = 1
            current_user.dir_sort_type = 'Seq_Msg_Num'
            opt = 1  

        if doc_id != '':
            current_user.request_status = 2
        else:
            current_user.doc_id = ''
            
        if params.path in ['/notices/', '/notices']:        
#            current_user.last_search_expression = ''.join(urlparse.parse_qs(params.query)['srch_argv'])
            current_user.request_status = 0
#            current_user.records = 0
#            current_user.current_page = 1
#            current_user.dir_sort_type = 'Seq_Msg_Num'
#            opt = 1   
        
        if current_user.request_status == 0:
            logging.info("==CSMS========= LIST RESULT REQUEST")
            if current_user.last_search_expression == '':
                http_client.fetch(CSMS_context_url, callback=self.on_fetch_results)
                return

            params = {

                        'srch_argv':current_user.last_search_expression.encode('utf-8'),
                        'page':current_user.current_page,
                        'sortby':current_user.dir_sort,
                        'by_type':current_user.dir_sort_type,
                        'sby':current_user.dir_sort_sby,
                        'btype':current_user.btype,
                        'opt':opt,
                     'image.y':12
                }
            headers = {
                   'User-Agent': user_agent,

                   } 
            url = url_concat('http://apps.cbp.gov/csms/csms.asp', params)

            req = tornado.httpclient.HTTPRequest(
                                              url=url,
                                              headers=headers
                                              )
            
            http_client.fetch(req, callback=self.on_fetch_results)
            
            
            
        elif current_user.request_status == 2:
            logging.info("==CSMS========= DOC VIEW REQUEST")
            
            if doc_id == '':
                doc_id = current_user.last_doc
            
            params = {'Recid':doc_id}
            headers = {
                   'User-Agent': user_agent,

                   } 
            url = url_concat('http://apps.cbp.gov/csms/viewmssg.asp', params)

            req = tornado.httpclient.HTTPRequest(
                                              url=url,
                                              headers=headers
                                              )

            http_client.fetch(req, callback=self.on_fetch_doc)
        elif current_user.request_status == 3:
            logging.info("==CSMS========= BACK TO LIST RESULT")
            current_user = self.current_user.csms
            diapason = current_user.getDiapason()
            current_user.request_status = 0
            current_user.doc_id = ''
            self.render(list_view_html_template, caption=caption, csms_url=CSMS_context_url, diapason=diapason, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), current_page=str(current_user.current_page),
                     btype=current_user.btype, result=current_user.last_content, sortby=current_user.dir_sort)     
        
        current_user.last_doc = doc_id     
            
            
    def on_fetch_doc(self, response):
        current_user = self.current_user.csms
        result = csms_doc_request(response, current_user, CSMS_context_url)

        diapason = ''
        self.render(doc_view_html_template, caption=caption, csms_url=CSMS_context_url, diapason=diapason, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), current_page=str(current_user.current_page),
                     btype=current_user.btype, result=result, sortby=current_user.dir_sort)
        
        
    def on_fetch_results(self, response):

        current_user = self.current_user.csms
        diapason = current_user.getDiapason()
        current_user.last_content = csms_main_request(response, current_user, CSMS_context_url)
        self.render(list_view_html_template, caption=caption, csms_url=CSMS_context_url, diapason=diapason, search_expression=current_user.last_search_expression,
                     records=str(current_user.records), current_page=str(current_user.current_page),
                     btype=current_user.btype, result=current_user.last_content, sortby=current_user.dir_sort)
        
    
    def post(self):

        current_user = self.current_user.csms

        if self.check_arguments("go"):
            current_user.current_page = 1
            current_user.request_status = 0
            current_user.records = 0
            current_user.last_search_expression = self.get_argument("qu")

        elif self.check_arguments("end"): 
            current_user.current_page = int(math.ceil(float(current_user.records) / float(10)))
            current_user.request_status = 0
            current_user.last_search_expression = self.get_argument("qu")

        elif self.check_arguments("next"): 
            current_user.current_page += 1
            current_user.request_status = 0
            current_user.last_search_expression = self.get_argument("qu")
            
        elif self.check_arguments("prev") and current_user.current_page > 1:             
            current_user.current_page -= 1
            current_user.request_status = 0
            current_user.last_search_expression = self.get_argument("qu")

        elif self.check_arguments("asc_pubdate"):                         
            current_user.dir_sort = 'date_published'
            current_user.dir_sort_type = 'pubdate'
            current_user.dir_sort_sby = 'asc'
            current_user.request_status = 0
            
        elif self.check_arguments("desc_pubdate"):             
            current_user.dir_sort = 'date_published'
            current_user.dir_sort_type = 'pubdate'
            current_user.dir_sort_sby = 'desc'
            current_user.request_status = 0

        elif self.check_arguments("asc_admin"):                         
            current_user.dir_sort = 'Seq_Msg_Num'
            current_user.dir_sort_type = 'admin'
            current_user.dir_sort_sby = 'asc'
            current_user.request_status = 0
        elif self.check_arguments("desc_admin"):             
            current_user.dir_sort = 'Seq_Msg_Num'
            current_user.dir_sort_type = 'pubdate'
            current_user.dir_sort_sby = 'desc'
            current_user.request_status = 0

        elif self.check_arguments("asc_title"):                         
            current_user.dir_sort = 'title'
            current_user.dir_sort_type = 'title'
            current_user.dir_sort_sby = 'asc'
            current_user.request_status = 0
        elif self.check_arguments("desc_title"):             
            current_user.dir_sort = 'title'
            current_user.dir_sort_type = 'title'
            current_user.dir_sort_sby = 'desc'
            current_user.request_status = 0
                          
        elif self.check_arguments("backtolist"):             
            current_user.request_status = 3
        if self.check_arguments("sortby"):
            current_user.dir_sort = self.get_argument("sortby")
            current_user.dir_sort_sby = 'desc'
                                
        if self.check_arguments("btype"):
            current_user.btype = self.get_argument("btype")                
        self.redirect(CSMS_context_url)              



class csmsDownloadHandler(BaseHandler):  # http://apps.cbp.gov/csms/docs/19913_848887382/Trade_Day_Registration_attachment.pdf
    @tornado.web.asynchronous
    def get(self, href=''):
        if href != '':
            self.set_header('Content-Type', 'application/octet-stream')
            http_client = httpclient.AsyncHTTPClient()
            http_client.fetch('http://apps.cbp.gov/csms/docs/' + href, callback=self.on_fetch_download)
    
    def on_fetch_download(self, response):      
            self.write(response.body)
            self.finish()
            
