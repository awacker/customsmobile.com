import urllib2, urllib
from bs4 import BeautifulSoup, Tag
import pymongo
import datetime
import settings
import time
import os
import traceback
from elasticsearch import Elasticsearch

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36'
CROSS_context_url = r'/cross'  
nas_path = 'cross_docs/' 
ps = 10
key_world = 'sincerely'  # 'sincerely', #'Munich',#
key_world1 = 'CATEGORY AND NOT sincerely'

global_cookie = ''

def error_message(msg):
    global error_contaner
    error_contaner.append(msg)
    print(msg)


def get_cross_list(p, k):
    global global_cookie
    http_path = 'http://rulings.cbp.gov/results.asp' 

    print('$$$ Read list on %s / cookie %s / page %s' % (http_path, global_cookie, p))

    try:
        params = {
                        'colSort':'1',
                        'dirSort':'desc',
                        'qu': k,
                        'ps': ps,
                        'p':str(p),
                }
        data = urllib.urlencode(params)
        req = urllib2.Request(http_path, headers={'User-Agent': user_agent, 'Cookie': global_cookie}, data=data)
        
        r = urllib2.urlopen(req, timeout=30)

        if 'Set-Cookie' in r.info():
            global_cookie = r.info()['Set-Cookie']
        
#        print r.read().decode('cp1252', 'ignore')
#        print '==================================='
        return r.read().decode('cp1252', 'ignore')
    except IOError as err:
        error_message("$$$ HTTP READ ERROR !!! Original message: {0}".format(err))
        return None
    
def get_doc_file(path):
    http_path = 'http://rulings.cbp.gov' + path

    print('######################### Read file %s' % http_path)
    try:
        directory = nas_path + http_path.split('/')[-3] + '/' + http_path.split('/')[-2]
        if not os.path.exists(directory):
            os.makedirs(directory)

        urllib.urlretrieve (http_path, directory + '/' + http_path.split('/')[-1])

    except IOError as err:
        error_message("$$$ HTTP READ ERROR !!! Original message: {0}".format(err))
        exit    


def get_doc_html_content(real_doc_id):
    global global_cookie
    http_path = 'http://rulings.cbp.gov/detail.asp' 

    print('######################### Read content for doc_id %s / cookie %s' % (real_doc_id, global_cookie))
    try:
        
        params = {'ru':real_doc_id, 'as':'pr'}
        data = urllib.urlencode(params)
        req = urllib2.Request(http_path, headers={'User-Agent': user_agent, 'Cookie': global_cookie}, data=data)
        
        r = urllib2.urlopen(req, timeout=30)
#        print r.info()['Set-Cookie']
    #    r_cookie = ''
        if 'Set-Cookie' in r.info():
            global_cookie = r.info()['Set-Cookie']
            
        soup = BeautifulSoup(r.read().decode('cp1252', 'ignore')).find("tr", {"class": ["detail_body", "printer"], })
        
        if soup == None:
            return None

        brLi = soup.findAll('br')
        try:
            for i in range(4): 
                brLi[i].extract()
        except:
            pass

        
        for doc in soup.findAll("a", {"target": ["detail", "_top", ]}):
            href = doc.get('href')
            if href[:11] == 'detail.asp?':
                doc_id = href.split('&')[0][14:]
                doc['href'] = CROSS_context_url + '/docview?doc_id=' + doc.getText()
                doc['target'] = ''
                
            
            
            
        return str(soup)

    except IOError as err:
        error_message("$$$ HTTP READ ERROR !!! Original message: {0}".format(err))


    
    
def save_item_to_db(page_Html, drop):
    path = ''
    soup = BeautifulSoup(page_Html)
    table = soup.find('table')
    
    
    paging_node = table.find('table')

    paging_sub_node = paging_node.find('td', {'class': 'pageCountHeader'})
    if paging_sub_node == None:
        return 0, -1, None
        

    paging_value = paging_sub_node.next.split()
    all_records = int(paging_value[3])
    last_record = int(paging_value[1].split('-')[1][:-1])
    
#    collection = get_db_collection(drop)
#    if collection == None: return

    l = []
    last_cookie = ''
    es = Elasticsearch()
    
    for tr in soup.findAll('table', {'class': 'results'})[1].contents[1:-1]:
        
        tdLi = tr.findAll("td", {"scope": ["row", ]})
        
        modifies_liks = []
        references_liks = []
        revokes_liks = []
        revoking_liks = []
        modifying_liks = []
        
        related_flag = 0
        for el in tdLi[4]:
            
            if isinstance(el, Tag):
                if el.has_attr('class') and el['class'][0] == 'modified_heading': related_flag = 1
                elif el.has_attr('class') and el['class'][0] == 'referenced_heading': related_flag = 2
                elif el.has_attr('class') and el['class'][0] == 'revoked_heading': related_flag = 3
                elif el.has_attr('class') and el['class'][0] == 'revoking_heading': related_flag = 4
                elif el.has_attr('class') and el['class'][0] == 'modifying_heading': related_flag = 5
                elif el.has_attr('target'):
                    if related_flag == 1: modifies_liks.append('<a href="%s">%s</a>' % (CROSS_context_url + '/docview?doc_id' + el['href'].split('&')[0][13:], el.getText()))
                    elif related_flag == 2: 
                        
                        references_liks.append('<a href="%s">%s</a>' % (CROSS_context_url + '/docview?doc_id' + el['href'].split('&')[0][13:], el.getText()))
                    elif related_flag == 3: revokes_liks.append('<a href="%s">%s</a>' % (CROSS_context_url + '/docview?doc_id' + el['href'].split('&')[0][13:], el.getText()))
                    elif related_flag == 4: revoking_liks.append('<a href="%s">%s</a>' % (CROSS_context_url + '/docview?doc_id' + el['href'].split('&')[0][13:], el.getText()))
                    elif related_flag == 5: modifying_liks.append('<a href="%s">%s</a>' % (CROSS_context_url + '/docview?doc_id' + el['href'].split('&')[0][13:], el.getText()))
      
        
        doc_id = tdLi[2].findAll("a")[1].getText().strip()
        real_doc_id = doc_id.split()
        if len(real_doc_id) > 0:
            real_doc_id = real_doc_id[len(real_doc_id) - 1].lower()
        
            doc_html_content = get_doc_html_content(real_doc_id)  # n257950 real_doc_id
        
            file_name = tdLi[2].findAll("a")[0]['href']

            d = tdLi[0].getText().split('/')
        
            try:
                dt = datetime.datetime(int(d[2]), int(d[0]), int(d[1]), 0, 0, 0)
            except:
                dt = atetime.datetime.now()
                pass
       
            try:
                category = tdLi[2].find('br').next
                category.split(' ')
            except:
                category = ''

            try:
                
                doc_object = {              
                    'date': dt,
                    'rank': float(tdLi[1].getText().split('%')[0]),
                    'category': category,
                    'doc_id': doc_id,
                    'real_doc_id': real_doc_id,
                    'tariff_no': '; '.join(['<a href="%s">%s</a>' % (a['href'], a.getText().strip()) for a in tdLi[2].findAll("a")[2:]]),
                    'ruling_reference': tdLi[3].getText(),
                    'related_modifies_liks': '; '.join(modifies_liks),
                    'related_references_liks': '; '.join(references_liks),
                    'related_revokes_liks': '; '.join(revokes_liks),
                    'related_revoking_liks': '; '.join(revoking_liks),
                    'related_modifying_liks': '; '.join(modifying_liks),
                    'doc_html_content': doc_html_content,
                    'file_name': file_name
                    }
                
                res = es.index(index="node1.cross", doc_type='ruling', id=doc_id, body=doc_object)
                print res['created']

            except Exception as err:
                error_message("-ERROR OF UPDATE !!!------ Original message: {0} \n {1} \n ".format(err, traceback.format_exc()))

    return all_records, last_record  # , collection
         
    

error_contaner = []
all_r = 2 + ps
last_r = 1
p_number = 1

while last_r < all_r and p_number < 50: 
    
    html = get_cross_list(p_number, key_world)


    if p_number == 1:
        d = True
    else:
        d = False
    if html:
        all_r, last_r = save_item_to_db(html, False)
   
        p_number += 1

    print last_r, all_r
        
while last_r < all_r and p_number < 10: 
    
    html = get_cross_list(p_number, key_world1)


    if p_number == 1:
        d = True
    else:
        d = False
    if html:
        all_r, last_r = save_item_to_db(html, False)
  
        p_number += 1

    print last_r, all_r
