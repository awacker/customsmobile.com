'''
Created on Jun 6, 2014

@author: awacker
'''
import os
from bs4 import BeautifulSoup
import lxml.etree as ET



def getContent(current_user_structure, page_Html, path):
    if current_user_structure.last_search_expression == '':
        return {'error': 'Please enter a search term'}
    if page_Html == None:
        return {'error': 'Source site is not currently available'}

    soup = BeautifulSoup(page_Html).find('table') 
    if soup == None: 
        return {'error': 'No documents matched your query'}
    
    l = []
    for tr in soup.findAll('tr'):
        tdLi = tr.findAll('td')
        l.append({
            'name': tdLi[0].getText(),
            'link': path + '/tableview/' + tdLi[0].find('a')['href'],
            'match': tdLi[-1].getText(),
        })
        
    current_user_structure.records = len(soup.findAll('tr'))
    if current_user_structure.records == 0: 
        return {'error': 'No documents matched your query'}
        
    return {'searchResults': l}
    
def getIndexContent(current_user_structure, page_Html, path):
    if page_Html == None:
        return {'error': 'Source site in not available currently'}

    soup = BeautifulSoup(page_Html).find('table') 
    if soup == None: 
        return {'error': 'No documents matched your query'}
    l = []
    for tr in soup.findAll('tr'):
        s = tr.getText().strip()
        if not s: continue
        if s.startswith('SECTION'):
            l.append([(s.split(':')[0], ':'.join(s.split(':')[1:]))])
        elif s.startswith('Chapter'):
            l[-1].append((s.split('\n')[0], path + '/tableview/' + tr.find('a')['href'] if tr.find('a') else '', '\n'.join(s.split('\n')[1:])))        
    return {'results': l}


def usitc_index_request(response, current_user_structure, path):
        
    page_Html = response.body
    
    return getIndexContent(current_user_structure, page_Html, path)

def usitc_main_request(response, current_user_structure, pat):
        
    page_Html = response.body
    
    return getContent(current_user_structure, page_Html, pat)


def usitc_table_request(response, current_user_structure):
    
    if response.code != 200:
        return '</br>Error retrieving document. Code %s' % response.code
    
    if response.body == None:
        return '</br>Document is empty'   
    
    
     
    xsl_path = os.path.join(os.path.dirname(__file__), "templates/usitc/usitc_main.xsl")  
    dom = ET.XML(response.body)
    xslt = ET.parse(xsl_path)
    transform = ET.XSLT(xslt)
    newdom = transform(dom)
    html_text = ET.tostring(newdom, pretty_print=True)
    
    soup = BeautifulSoup(html_text)
    [s.extract() for s in soup({'script', 'style', 'hr'})]
    while True: 
        h = soup.find({'h1', 'h2'})
        if not h:
            break
        h.name = 'h3'

    return soup





