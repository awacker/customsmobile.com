import os
import sys
import subprocess
import fcntl
import signal
import time

import logging

import tornado.httpserver
import tornado.web
import os.path
from tornado.options import define, options

from handlers import IndexHandler
from ports_handlers import PORTS_context_url, portsMainHandler
from usitc_handlers import USITC_context_url, usitcMainHandler
from cross_handlers import CROSS_context_url, DownloadHandler, crossMainHandler, crossHandler_doc
from csms_handlers import CSMS_context_url, csmsMainHandler, csmsDownloadHandler

DEBUG = False
DEBUG = True

define("port", default=8880, help="run on the given port", type=int)

class StaticPageHandler(tornado.web.RequestHandler):

    def initialize(self, caption):
        self.caption = caption

    @tornado.web.asynchronous
    @tornado.gen.engine
    def get(self):
        self.render('static' + self.request.uri, caption=self.caption),

class Application(tornado.web.Application):
    def __init__(self):
        images_path = os.path.join(os.path.dirname(__file__), "images")
        handlers = [
            (r"/", IndexHandler),

            (PORTS_context_url + r'/?', portsMainHandler),
            (PORTS_context_url + r"/contact/ports/(.*)", portsMainHandler),
                        
            (USITC_context_url + r'/?', usitcMainHandler),
            (USITC_context_url + r"/tableview/(.*)", usitcMainHandler),

            (CSMS_context_url + r'/?', csmsMainHandler),
            (CSMS_context_url + r"/docview/(.*)", csmsMainHandler),
            (CSMS_context_url + r"/attachment/(.*)", csmsDownloadHandler),
  
            (CROSS_context_url + r'/?', crossMainHandler),
            (CROSS_context_url + r"/docview(.*)", crossHandler_doc),
  #          (CROSS_context_url + r"/docheader(.*)", crossHandler_doc_header),
            (CROSS_context_url + r"/docdownload/(.*)", DownloadHandler),
            
            (r"/images/(.*)", tornado.web.StaticFileHandler, {"path": images_path}),

# # These redirect to the above class StaticPageHandler.
            
            (r'/about.html', StaticPageHandler, {"caption": "ABOUT"}),
            (r'/disclaimer.html', StaticPageHandler, {"caption": "DISCLAIMER"}),
            (r'/privacy.html', StaticPageHandler, {"caption": "PRIVACY"}),
            (r'/bio.html', StaticPageHandler, {"caption": "BIO"}),
            (r'/updates.html', StaticPageHandler, {"caption": "updates"}),

# #Below remmed by CCB.  These redirect to a new URL (/static...).  Because we have the above StaticPageHandler, there's no need for the particular redirects.
# #            (r"/about.html", tornado.web.RedirectHandler, {"url": "/static/about.html"}),
            (r"/regs.html", tornado.web.RedirectHandler, {"url": "/static/regs.php"}),
        ]
        settings = dict(

            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            images_path=images_path,

            
        )
        tornado.web.Application.__init__(self, handlers, **settings)



      
##### MAIN =========================================================================   
def main():

    if DEBUG == False:    
        LOG_FILE = os.path.join(os.path.dirname(__file__), "logs/_app_info.log")  
        LOG_LEVEL = 'INFO'
     
        file_handler = logging.handlers.RotatingFileHandler(
                                                            filename=LOG_FILE, mode='a+',
                                                            maxBytes=1000000,
                                                            backupCount=2)  
        file_handler.setLevel(getattr(logging, LOG_LEVEL))
        file_handler.setFormatter(
                                  logging.Formatter('%(asctime)s\t%(levelname)-8s %(message)s',
                                                    datefmt='%d-%m-%Y %H:%M:%S'))
     
        logging.getLogger('').addHandler(file_handler)

    tornado.options.parse_command_line()
        
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)    
    tornado.ioloop.IOLoop.instance().start()

# ## DON'T TOUCH - FOR DEBUG

if DEBUG and __name__ == "__main__":    
    main()

##### daemon =========================================================================   

COMMANDS = ['start', 'stop', 'restart']
PID_FNAME = '/tmp/' + '_'.join((os.path.abspath(__file__).strip('/').split('/'))) + '.pid'
HOST = 'localhost'

def daemon():
    logging.critical('--- SERVER (RE)STARTED')
    f = open(PID_FNAME, 'w')
    fcntl.flock(f.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
    f.write('%-12i' % os.getpid())
    f.flush()    
    main()


def alegry_started():

    if not os.path.exists(PID_FNAME):
        f = open(PID_FNAME, "w")
        f.write('0')
        f.flush()
        f.close()

    f = open(PID_FNAME, 'r+')
    try:
        fcntl.flock(f.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError:
        started = int(f.read())
    else:
        started = 0
    f.close()
    return started

def start():
    started = alegry_started()
    if not started:
        env = os.environ
        env['PYTHONPATH'] = ', '.join(sys.path)
        cmd = 'python2.7 ' + os.path.abspath(__file__) + ' daemon'
        pid = subprocess.Popen(
                           cmd,
                           shell=True,
                           env=env,
                           ).pid
        print 'Server started at port %s (pid: %i)...' % (options.port, pid)
    else:
        print 'Server alegry started (pid: %i)' % started

def stop():
    started = alegry_started()
    if started:
        os.kill(started, signal.SIGKILL)
        print 'Server stoped (pid %i)' % started
    else:
        print 'Server not started'


def restart():
    stop()
    time.sleep(1)
    start()


if len(sys.argv) == 2 and sys.argv[1] in (COMMANDS + ['daemon']):
    cmd = sys.argv[1]
    globals()[cmd]()
else:
    print 'Error: invalid command'
    print 'Usage: python main.py {%s}.' % '|'.join(COMMANDS)

