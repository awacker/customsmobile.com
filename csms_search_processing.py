from bs4 import BeautifulSoup
import urlparse

def getContent(current_user_structure, page_Html, path):
    if current_user_structure.last_search_expression == '':
#        return {'error': 'Please enter a search term above'}
        return {'error': 'Your search, your business: visit our Facebook page to read about what we\'re doing to actively protect your privacy.'}
    if page_Html == None:
        return {'error': 'Source site is not currently available'}

    soup = BeautifulSoup(page_Html)
    paging_table = soup.find('table', {'class': 'pageCountHeader'})
    if paging_table == None:
        current_user_structure.records = 0
        return {'error': 'No documents matched your query'}
    paging_subtable = paging_table.find('table', {'class': 'pageCountHeader'})
    if paging_subtable == None: 
        return {'error': 'No documents matched your query'}
    paging_sub_node = paging_subtable.find('td', {'align': 'right'})
    if paging_sub_node == None:
        return {'error': 'No documents matched your query'}
    paging_value = paging_sub_node.next.split()
    current_user_structure.records = int(paging_value[5])
    l = []

    for tr in soup.find('table', {'class': 'results'}).findAll('tr')[1:]:
        tdLi = tr.findAll("td", {"scope": ["row", ]})
        getRecid = lambda href: ''.join(urlparse.parse_qs(urlparse.urlparse(href).query)['Recid'])
        l.append({
            'date': ' at '.join(tdLi[0].stripped_strings),
            'id': tdLi[1].getText().strip(),
            'title': tdLi[2].find('b').getText(),
            'desc': tdLi[2].find('br').next,
            'group': tdLi[3].getText(),
            'related': '; '.join(['<a href="%s">%s</a>' % ('?' + urlparse.urlparse(r['href']).query, r.getText()) for r in tdLi[4].findAll("a")]),
            'link': path + '/docview/' + getRecid(tdLi[1].find("a")['href'])
        })
        
    return {'results': l}



def csms_main_request(response, current_user_structure, path):
    page_Html = response.body
    
    return getContent(current_user_structure, page_Html, path)


def csms_doc_request(response, current_user_structure, path):

    if response.body == None:
        return {'error': 'Document is empty'}
    
    soup = BeautifulSoup(response.body).find('form', {'action': 'viewmsg.asp'}).find('table')
    d = {
        'id': soup.find('span', {'class': 'article_title'}).getText().split('#')[1].strip(),
        'title': soup.find('span', {'class': 'article_subtitle2'}).getText(),
        'date': soup.findAll('span', {'class': 'about_text'})[0].getText(),
        'group': soup.findAll('span', {'class': 'about_text'})[1].getText(),
        'content': soup.find('pre').getText()
    }
    
    return d






